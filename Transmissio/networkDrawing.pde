// Transmissio - version 1.1

/**
 * Copyright Gael Beaunée (April 10th 2019)
 *
 * gael.beaunee@gmail.com
 *
 * This software is a computer program whose purpose is to visualize
 * temporal graph data (network) combined with geographic information
 * (node coordinates).
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 */


void networkDrawing() {

  strokeCap(ROUND);
  cursor(HAND);

  //     ___                       ___                                   ___
  //    /  /\          ___        /  /\          ___       ___          /  /\
  //   /  /:/_        /  /\      /  /::\        /  /\     /  /\        /  /:/
  //  /  /:/ /\      /  /:/     /  /:/\:\      /  /:/    /  /:/       /  /:/
  // /  /:/ /::\    /  /:/     /  /:/~/::\    /  /:/    /__/::\      /  /:/  ___
  ///__/:/ /:/\:\  /  /::\    /__/:/ /:/\:\  /  /::\    \__\/\:\__  /__/:/  /  /\
  //\  \:\/:/~/:/ /__/:/\:\   \  \:\/:/__\/ /__/:/\:\      \  \:\/\ \  \:\ /  /:/
  // \  \::/ /:/  \__\/  \:\   \  \::/      \__\/  \:\      \__\::/  \  \:\  /:/
  //  \__\/ /:/        \  \:\   \  \:\           \  \:\     /__/:/    \  \:\/:/
  //    /__/:/          \__\/    \  \:\           \__\/     \__\/      \  \::/
  //    \__\/                     \__\/                                 \__\/

  if (REFRESH_VIZ) {
    REFRESH_VIZ = false;

    // clear the screen
    background(viz_properties_color.get("col_background"));

    // draw boundaries
    boundariesDrawing();

    // ingoing and outgoing flow have to be cleared before to refresh the visualization, in order to take into account the right time period
    for (String id : list_id_of_node_with_coordinates) {
      hm_nodes.get(id).ingoing_flow = 0;
      hm_nodes.get(id).outgoing_flow = 0;
      hm_nodes.get(id).ingoing_flow_from_na = 0;
      hm_nodes.get(id).outgoing_flow_to_na = 0;
    }

    // update edges (link) information
    for (String id : list_id_of_node_with_coordinates) {
      if (LINKS_ARE_DRAWN) {
        HashMap<String, HashMap<String, Float>> inventory = new HashMap<String, HashMap<String, Float>>();

        for (int the_timestep = timestep_start_viz; the_timestep <= timestep_end_viz; the_timestep = the_timestep+1) {
          HashMap<String, Float> movs = hm_nodes.get(id).hm_timestep_hm_destination_flow.get(the_timestep);
          HashMap<String, HashMap<String, Float>> movs_type = hm_nodes.get(id).hm_timestep_hm_destination_hm_flowType_flow.get(the_timestep);
          if (movs != null) {
            for (String dest : movs.keySet()) {
              hm_nodes.get(id).outgoing_flow += movs.get(dest);
              hm_nodes.get(dest).ingoing_flow += movs.get(dest);
              for (String type : movs_type.get(dest).keySet()) {
                if (inventory.containsKey(dest)) {
                  if (inventory.get(dest).containsKey(type)) {
                    inventory.get(dest).put(type, inventory.get(dest).get(type) + movs_type.get(dest).get(type));
                  } else {
                    inventory.get(dest).put(type, movs_type.get(dest).get(type));
                  }
                } else {
                  HashMap<String, Float> sub_inventory = new HashMap<String, Float>();
                  sub_inventory.put(type, movs_type.get(dest).get(type));
                  inventory.put(dest, sub_inventory);
                }
              }
            }
          }
          if (hm_nodes.get(id).hm_timestep_flow_from_na.containsKey(the_timestep)) {
            hm_nodes.get(id).ingoing_flow_from_na += hm_nodes.get(id).hm_timestep_flow_from_na.get(the_timestep);
          }
          if (hm_nodes.get(id).hm_timestep_flow_to_na.containsKey(the_timestep)) {
            hm_nodes.get(id).outgoing_flow_to_na += hm_nodes.get(id).hm_timestep_flow_to_na.get(the_timestep);
          }
        }


        for (String dest : inventory.keySet()) {
          for (String type : inventory.get(dest).keySet()) {
            float beginX = hm_nodes.get(id).x;
            float beginY = hm_nodes.get(id).y;
            float endX = hm_nodes.get(dest).x;
            float endY = hm_nodes.get(dest).y;
            float destOccurence = inventory.get(dest).get(type);

            color theColor = viz_properties_HMcolor.get("col_link_flow").get(type);
            noFill();
            strokeWeight(map(min(destOccurence, viz_properties_float.get("threshold_max_flow_size_strokeWeight")),
              viz_properties_float.get("threshold_min_flow_size_strokeWeight"),
              viz_properties_float.get("threshold_max_flow_size_strokeWeight"),
              viz_properties_float.get("threshold_link_strokeWeight_min"),
              viz_properties_float.get("threshold_link_strokeWeight_max"))
              );
            stroke(theColor, map(min(destOccurence, viz_properties_float.get("threshold_max_flow_size_strokeTransparency")),
              viz_properties_float.get("threshold_min_flow_size_strokeTransparency"),
              viz_properties_float.get("threshold_max_flow_size_strokeTransparency"),
              viz_properties_float.get("threshold_link_strokeTransparency_min"),
              viz_properties_float.get("threshold_link_strokeTransparency_max"))
              );

            if (destOccurence >= viz_properties_float.get("threshold_min_flow_to_draw")) {
              if (LINK_WITH_ARROW) {
                drawCurvedArrow(beginX, beginY, endX, endY);
              } else {
                //line(beginX, beginY, endX, endY);
                drawCurvedLine(beginX, beginY, endX, endY);
              }
            }
          }
        }
      } else {

        if (focus_node != null) {
          HashMap<String, HashMap<String, Float>> inventory = new HashMap<String, HashMap<String, Float>>();

          for (int the_timestep = timestep_start_viz; the_timestep <= timestep_end_viz; the_timestep = the_timestep+1) {
            HashMap<String, Float> movs = hm_nodes.get(id).hm_timestep_hm_destination_flow.get(the_timestep);
            HashMap<String, HashMap<String, Float>> movs_type = hm_nodes.get(id).hm_timestep_hm_destination_hm_flowType_flow.get(the_timestep);
            if (movs != null) {
              for (String dest : movs.keySet()) {
                hm_nodes.get(id).outgoing_flow += movs.get(dest);
                hm_nodes.get(dest).ingoing_flow += movs.get(dest);
                if ((id.equals(focus_node.node_id) == true) | (dest.equals(focus_node.node_id) == true)) {
                  for (String type : movs_type.get(dest).keySet()) {
                    if (inventory.containsKey(dest)) {
                      if (inventory.get(dest).containsKey(type)) {
                        inventory.get(dest).put(type, inventory.get(dest).get(type) + movs_type.get(dest).get(type));
                      } else {
                        inventory.get(dest).put(type, movs_type.get(dest).get(type));
                      }
                    } else {
                      HashMap<String, Float> sub_inventory = new HashMap<String, Float>();
                      sub_inventory.put(type, movs_type.get(dest).get(type));
                      inventory.put(dest, sub_inventory);
                    }
                  }
                }
              }
            }
            if (hm_nodes.get(id).hm_timestep_flow_from_na.containsKey(the_timestep)) {
              hm_nodes.get(id).ingoing_flow_from_na += hm_nodes.get(id).hm_timestep_flow_from_na.get(the_timestep);
            }
            if (hm_nodes.get(id).hm_timestep_flow_to_na.containsKey(the_timestep)) {
              hm_nodes.get(id).outgoing_flow_to_na += hm_nodes.get(id).hm_timestep_flow_to_na.get(the_timestep);
            }
          }

          for (String dest : inventory.keySet()) {
            for (String type : inventory.get(dest).keySet()) {
              float beginX = hm_nodes.get(id).x;
              float beginY = hm_nodes.get(id).y;
              float endX = hm_nodes.get(dest).x;
              float endY = hm_nodes.get(dest).y;
              float destOccurence = inventory.get(dest).get(type);

              strokeWeight(map(min(destOccurence, viz_properties_float.get("threshold_max_flow_size_strokeWeight")),
                viz_properties_float.get("threshold_min_flow_size_strokeWeight"),
                viz_properties_float.get("threshold_max_flow_size_strokeWeight"),
                viz_properties_float.get("threshold_link_strokeWeight_min"),
                viz_properties_float.get("threshold_link_strokeWeight_max"))
                );

              if (id.equals(focus_node.node_id) == true) { // outgoing flow
                color theColor = viz_properties_HMcolor.get("col_link_outgoing_flow_for_focus_node").get(type);
                noFill();
                stroke(theColor, map(min(destOccurence, viz_properties_float.get("threshold_max_flow_size_strokeTransparency")),
                  viz_properties_float.get("threshold_min_flow_size_strokeTransparency"),
                  viz_properties_float.get("threshold_max_flow_size_strokeTransparency"),
                  viz_properties_float.get("threshold_link_strokeTransparency_min_for_focus_node"),
                  viz_properties_float.get("threshold_link_strokeTransparency_max_for_focus_node"))
                  );
              } else { // ingoing flow
                color theColor = viz_properties_HMcolor.get("col_link_ingoing_flow_for_focus_node").get(type);
                noFill();
                stroke(theColor, map(min(destOccurence, viz_properties_float.get("threshold_max_flow_size_strokeTransparency")),
                  viz_properties_float.get("threshold_min_flow_size_strokeTransparency"),
                  viz_properties_float.get("threshold_max_flow_size_strokeTransparency"),
                  viz_properties_float.get("threshold_link_strokeTransparency_min_for_focus_node"),
                  viz_properties_float.get("threshold_link_strokeTransparency_max_for_focus_node"))
                  );
              }

              if (! id.equals(dest)) {
                if (destOccurence >= viz_properties_float.get("threshold_min_flow_to_draw_for_focus_node")) {
                  if (LINK_WITH_ARROW) {
                    drawCurvedArrow(beginX, beginY, endX, endY);
                  } else {
                    //line(beginX, beginY, endX, endY);
                    drawCurvedLine(beginX, beginY, endX, endY);
                  }
                }
              } else {
              } // if mov occured inside the same node
            }
          }
        } else {
          for (int the_timestep = timestep_start_viz; the_timestep <= timestep_end_viz; the_timestep = the_timestep+1) {
            HashMap<String, Float> movs = hm_nodes.get(id).hm_timestep_hm_destination_flow.get(the_timestep);
            if (movs != null) {
              for (String dest : movs.keySet()) {
                hm_nodes.get(id).outgoing_flow += movs.get(dest);
                hm_nodes.get(dest).ingoing_flow += movs.get(dest);
              }
            }
            if (hm_nodes.get(id).hm_timestep_flow_from_na.containsKey(the_timestep)) {
              hm_nodes.get(id).ingoing_flow_from_na += hm_nodes.get(id).hm_timestep_flow_from_na.get(the_timestep);
            }
            if (hm_nodes.get(id).hm_timestep_flow_to_na.containsKey(the_timestep)) {
              hm_nodes.get(id).outgoing_flow_to_na += hm_nodes.get(id).hm_timestep_flow_to_na.get(the_timestep);
            }
          }
        }
      }
    }

    // nodes
    if (NODES_ARE_DRAWN) {
      max_of_in_and_out_flow = 0;
      for (String id : list_id_of_node_with_coordinates) {
        float the_current_node_in_and_out_flow = hm_nodes.get(id).ingoing_flow + hm_nodes.get(id).outgoing_flow + hm_nodes.get(id).ingoing_flow_from_na + hm_nodes.get(id).outgoing_flow_to_na;
        max_of_in_and_out_flow = max(max_of_in_and_out_flow, the_current_node_in_and_out_flow);
      }
      //println(max_of_in_and_out_flow);
      max_of_in_and_out_flow = min(viz_properties_float.get("threshold_for_the_max_of_in_and_out_flow"), max_of_in_and_out_flow);
      println(max_of_in_and_out_flow);

      if (focus_node != null) {
        fill(viz_properties_color.get("col_focus_node_halo"), viz_properties_float.get("focus_node_halo_transparency"));
        noStroke();
        float the_focus_node_in_and_out_flow = focus_node.ingoing_flow + focus_node.ingoing_flow_from_na + focus_node.outgoing_flow + focus_node.outgoing_flow_to_na;
        float diam = 2.*sqrt(((min(the_focus_node_in_and_out_flow, max_of_in_and_out_flow) / max_of_in_and_out_flow) * (sq(viz_properties_float.get("node_diameter_max")/2.)*PI))/PI);
        //diam = max(diam, 1.);
        diam += viz_properties_float.get("focus_node_halo_size");
        ellipse(focus_node.x, focus_node.y, diam, diam);
      }
      for (String id : list_id_of_node_with_coordinates) {
        hm_nodes.get(id).draw();
      }

      //// draw Nantes
      //float longitude_nantes = -1.553621;
      //float latitude_nantes = 47.218371;
      //float proj_longitude_nantes = ((longitude_nantes-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
      //float proj_latitude_nantes = ((latitude_nantes-mean_data_latitude)/360.)*TWO_PI;
      //float posX_nantes = map(proj_longitude_nantes, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
      //float posY_nantes = map(proj_latitude_nantes, min_proj_latitude, max_proj_latitude, height-margeY, margeY);
      //fill(10, 240);
      //noStroke();
      //ellipse(posX_nantes, posY_nantes, viz_properties_float.get("node_diameter_max") / 3., viz_properties_float.get("node_diameter_max") / 3.);
    }

    // legend
    if (LEGEND_IS_DRAWN) {
      if (focus_node != null) {
        // TODO: print informations on selected node
        drawLegendForFocusNode();
      } else {
        // TODO: draw the legend here
        drawLegendGeneral();
      }
    }
  }


  //     _____                      ___           ___           ___                       ___
  //    /  /::\         ___        /__/\         /  /\         /__/\        ___          /  /\
  //   /  /:/\:\       /__/|       \  \:\       /  /::\       |  |::\      /  /\        /  /:/
  //  /  /:/  \:\     |  |:|        \  \:\     /  /:/\:\      |  |:|:\    /  /:/       /  /:/
  // /__/:/ \__\:|    |  |:|    _____\__\:\   /  /:/~/::\   __|__|:|\:\  /__/::\      /  /:/  ___
  // \  \:\ /  /:/  __|__|:|   /__/::::::::\ /__/:/ /:/\:\ /__/::::| \:\ \__\/\:\__  /__/:/  /  /\
  //  \  \:\  /:/  /__/::::\   \  \:\~~\~~\/ \  \:\/:/__\/ \  \:\~~\__\/    \  \:\/\ \  \:\ /  /:/
  //   \  \:\/:/      ~\~~\:\   \  \:\  ~~~   \  \::/       \  \:\           \__\::/  \  \:\  /:/
  //    \  \::/         \  \:\   \  \:\        \  \:\        \  \:\          /__/:/    \  \:\/:/
  //     \__\/           \__\/    \  \:\        \  \:\        \  \:\         \__\/      \  \::/
  //                               \__\/         \__\/         \__\/                     \__\/

  // Dynamic graph
  if (DYNAMIC_VIZ) {
    strokeCap(ROUND);

    // clear the screen
    background(viz_properties_color.get("col_background"));

    // draw boundaries
    boundariesDrawing();

    float progress_of_the_mov = mov_step_index_in_dynamic_viz/viz_properties_float.get("total_number_of_step_in_one_mov_for_dynamic_viz");

    // draw nodes and movs
    for (String id : list_id_of_node_with_coordinates) {
      // nodes
      if (NODES_ARE_DRAWN) {
        hm_nodes.get(id).draw();
      }
      // movs
      if (hm_nodes.get(id).hm_timestep_hm_destination_flow.containsKey(current_timestep_in_dynamic_viz)) {
        HashMap<String, HashMap<String, Float>> inventory =  hm_nodes.get(id).hm_timestep_hm_destination_hm_flowType_flow.get(current_timestep_in_dynamic_viz);
        for (String dest : inventory.keySet()) {
          for (String type : inventory.get(dest).keySet()) {
            float beginX = hm_nodes.get(id).x;
            float beginY = hm_nodes.get(id).y;
            float endX = hm_nodes.get(dest).x;
            float endY = hm_nodes.get(dest).y;
            PVector d = new PVector(endX - beginX, endY - beginY);
            d.normalize();
            float dist = dist(beginX, beginY, endX, endY);
            float angle = atan2(d.y, d.x);
            // compute the position of the particle along the curved line
            float pointX = bezierPoint(0, 0+1+(dist*0.25), 0+dist-1-(dist*0.25), dist, progress_of_the_mov);
            float pointY = bezierPoint(0, 0-1-(dist*0.1), 0-1-(dist*0.1), 0, progress_of_the_mov);
            // get the size of the mov
            float destOccurence = inventory.get(dest).get(type);
            // draw the particle
            if (destOccurence >= viz_properties_float.get("threshold_min_flow_to_draw_for_dynamic_viz")) {
              if (beginX!=endX & beginY!=endY) {
                color the_particle_color = lerpColor(viz_properties_HMcolor.get("col_outgoing_particle_for_dynamic_viz").get(type), viz_properties_HMcolor.get("col_ingoing_particle_for_dynamic_viz").get(type), progress_of_the_mov);
                stroke(the_particle_color, viz_properties_float.get("particle_color_transparency"));
                noFill();
                float the_particle_size = map(min(destOccurence, viz_properties_float.get("threshold_max_flow_size_for_dynamic_viz")),
                  viz_properties_float.get("threshold_min_flow_to_draw_for_dynamic_viz"),
                  viz_properties_float.get("threshold_max_flow_size_for_dynamic_viz"),
                  viz_properties_float.get("particle_min_size_for_dynamic_viz"),
                  viz_properties_float.get("ratio_between_the_min_and_max_particle_area_for_dynamic_viz")*viz_properties_float.get("particle_min_size_for_dynamic_viz"));
                //float the_particle_diam = 2.*sqrt(((min(the_particle_size,viz_properties_float.get("threshold_max_flow_size_for_dynamic_viz"))/viz_properties_float.get("threshold_max_flow_size_for_dynamic_viz")) * (sq(viz_properties_float.get("ratio_between_the_min_and_max_particle_area_for_dynamic_viz")*viz_properties_float.get("particle_min_size_for_dynamic_viz")/2.)*PI))/PI);
                //float the_particle_diam = 2.*sqrt((min(the_particle_size,viz_properties_float.get("threshold_max_flow_size_for_dynamic_viz"))/viz_properties_float.get("threshold_max_flow_size_for_dynamic_viz")) * sq(viz_properties_float.get("ratio_between_the_min_and_max_particle_area_for_dynamic_viz")*viz_properties_float.get("particle_min_size_for_dynamic_viz")/2.));
                float the_particle_diam = 2.*sqrt(the_particle_size * sq(viz_properties_float.get("particle_min_size_for_dynamic_viz")/2.));
                strokeWeight(the_particle_diam);
                strokeCap(ROUND);
                pushMatrix();
                translate(beginX, beginY);
                rotate(angle);
                point(pointX, pointY);
                popMatrix();
              } //else {} // if mov occured inside the same node
            }
          }
        }
      }
    }

    //// draw Nantes
    //float longitude_nantes = -1.553621;
    //float latitude_nantes = 47.218371;
    //float proj_longitude_nantes = ((longitude_nantes-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
    //float proj_latitude_nantes = ((latitude_nantes-mean_data_latitude)/360.)*TWO_PI;
    //float posX_nantes = map(proj_longitude_nantes, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
    //float posY_nantes = map(proj_latitude_nantes, min_proj_latitude, max_proj_latitude, height-margeY, margeY);
    //fill(10, 240);
    //noStroke();
    //ellipse(posX_nantes, posY_nantes, viz_properties_float.get("node_diameter_max") / 3., viz_properties_float.get("node_diameter_max") / 3.);

    // print the current timestep
    if (viz_properties_float.get("text_size_time_step") > 0) {
      textFont(THE_FONT, viz_properties_float.get("text_size_time_step"));
      fill(100);
      textAlign(RIGHT, CENTER);
      text(str(current_timestep_in_dynamic_viz), width-20, height-30);
    }

    // if saving the dynamic viz
    if (SAVE_DYNAMIC_VIZ) {
      saveFrame(DATA_FOLDER_PATH + "/dyn/frames/" + viz_properties_string.get("network_file_name") + "_frame_" + nf(frame_number_in_dynamic_viz, amplitude_of_the_numer_of_frames_for_dynamic_viz) + ".png");
      //PImage img;
      ////img = get(round(margeX), round(margeY), round(width - 2*(margeX)), round(height - 2*(margeY)));
      //img = get(0, 0, width, height);
      //img.save(DATA_FOLDER_PATH + "/dyn/frames/" + viz_properties_string.get("network_file_name") + "_frame_" + nf(frame_number_in_dynamic_viz, amplitude_of_the_numer_of_frames_for_dynamic_viz) + ".png");
      // update the frame counter
      frame_number_in_dynamic_viz ++;
    }

    // update the step index for the current mov
    mov_step_index_in_dynamic_viz++;
    // update variable at the end of the mov (after last stpe of the mov)
    if (mov_step_index_in_dynamic_viz > viz_properties_float.get("total_number_of_step_in_one_mov_for_dynamic_viz")) {
      mov_step_index_in_dynamic_viz = 1;
      current_timestep_in_dynamic_viz++;
      // Stop the dynamic viz if the last mov was reached
      if (current_timestep_in_dynamic_viz > last_timestep_in_dynamic_viz) {
        DYNAMIC_VIZ = false;
        REFRESH_VIZ = true;
      }
    }
  }
}

//void boundariesDrawing() {
//  if (viz_properties_string.get("boundaries_geojson_file").equals("NA") == false) {
//    shape(areaBoundariesShape);
//  }
//}

void addBoundariesVertex(float latitude, float longitude) {
  float proj_latitude = latitude;
  float proj_longitude = longitude;
  if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
    proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
    proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
  } else
    if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
      proj_latitude = latitude;
      proj_longitude = longitude;
    } else {
      println("Not a valid coordinates format!");
    }
  //
  float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
  float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);
  //println("coord", posX, posY);
  vertex(posX, posY);
}

void boundariesDrawing() {
  // original boundariesDrawing function replace by this one, as there are some issues with the rendering of pre-built PShapes
  if (viz_properties_string.get("boundaries_geojson_file").equals("NA") == false) {
    for (int i = 0; i < boundaries_geojson.size(); i++) {
      String type_of_polygon = boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getString("type");
      if (type_of_polygon.equals("MultiPolygon") == true) { // MultiPolygon
        for (int j = 0; j < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").size(); j++) {
          // Build the PShape
          beginShape();
          //
          fill(viz_properties_color.get("col_boundaries_shape_fill"));
          //
          if (viz_properties_float.get("boundaries_shape_strokeWeight") > 0.0) {
            strokeWeight(viz_properties_float.get("boundaries_shape_strokeWeight"));
            stroke(viz_properties_color.get("col_boundaries_shape_stroke"));
          } else {
            noStroke();
          }
          //
          int k = 0;
          for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).size(); l++) {
            float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(0);
            float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(1);
            addBoundariesVertex(latitude, longitude);
          }
          if (boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).size() > 1) {
            for (k = 1; k < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).size(); k++) {
              beginContour();
              for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).size(); l++) {
                float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(0);
                float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(1);
                addBoundariesVertex(latitude, longitude);
              }
              endContour();
            }
          }
          //
          endShape(CLOSE);
        }
      } else if (type_of_polygon.equals("Polygon") == true) { // Polygon
        // Build the PShape
        beginShape();
        //
        fill(viz_properties_color.get("col_boundaries_shape_fill"));
        //
        if (viz_properties_float.get("boundaries_shape_strokeWeight") > 0.0) {
          strokeWeight(viz_properties_float.get("boundaries_shape_strokeWeight"));
          stroke(viz_properties_color.get("col_boundaries_shape_stroke"));
        } else {
          noStroke();
        }
        //
        int j = 0;
        for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).size(); l++) {
          float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(0);
          float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(1);
          addBoundariesVertex(latitude, longitude);
        }
        if (boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").size() > 1) {
          for (j = 1; j < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").size(); j++) {
            beginContour();
            for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).size(); l++) {
              float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(0);
              float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(1);
              addBoundariesVertex(latitude, longitude);
            }
            endContour();
          }
        }
        //
        endShape(CLOSE);
      }
    }
  }
}
