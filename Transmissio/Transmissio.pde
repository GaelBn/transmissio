// Transmissio - version 1.1

/**
 * Copyright Gael Beaunée (April 10th 2019)
 *
 * gael.beaunee@gmail.com
 *
 * This software is a computer program whose purpose is to visualize
 * temporal graph data (network) combined with geographic information
 * (node coordinates).
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 */


//                  ___           ___         ___           ___
//    ___          /__/\         /  /\       /  /\         /  /\          ___
//   /  /\        |  |::\       /  /::\     /  /::\       /  /::\        /  /\
//  /  /:/        |  |:|:\     /  /:/\:\   /  /:/\:\     /  /:/\:\      /  /:/
// /__/::\      __|__|:|\:\   /  /:/~/:/  /  /:/  \:\   /  /:/~/:/     /  /:/
// \__\/\:\__  /__/::::| \:\ /__/:/ /:/  /__/:/ \__\:\ /__/:/ /:/___  /  /::\
//    \  \:\/\ \  \:\~~\__\/ \  \:\/:/   \  \:\ /  /:/ \  \:\/:::::/ /__/:/\:\
//     \__\::/  \  \:\        \  \::/     \  \:\  /:/   \  \::/~~~~  \__\/  \:\
//     /__/:/    \  \:\        \  \:\      \  \:\/:/     \  \:\           \  \:\
//     \__\/      \  \:\        \  \:\      \  \::/       \  \:\           \__\/
//                 \__\/         \__\/       \__\/         \__\/

import processing.pdf.*;

import java.util.*;
import java.util.Date;
import java.text.SimpleDateFormat;

boolean RANGE_WAS_UPDATED = false;


//      ___         ___           ___           ___           ___
//     /  /\       /  /\         /  /\         /  /\         /__/\
//    /  /::\     /  /::\       /  /::\       /  /::\       |  |::\
//   /  /:/\:\   /  /:/\:\     /  /:/\:\     /  /:/\:\      |  |:|:\
//  /  /:/~/:/  /  /:/~/::\   /  /:/~/:/    /  /:/~/::\   __|__|:|\:\
// /__/:/ /:/  /__/:/ /:/\:\ /__/:/ /:/___ /__/:/ /:/\:\ /__/::::| \:\
// \  \:\/:/   \  \:\/:/__\/ \  \:\/:::::/ \  \:\/:/__\/ \  \:\~~\__\/
//  \  \::/     \  \::/       \  \::/~~~~   \  \::/       \  \:\
//   \  \:\      \  \:\        \  \:\        \  \:\        \  \:\
//    \  \:\      \  \:\        \  \:\        \  \:\        \  \:\
//     \__\/       \__\/         \__\/         \__\/         \__\/

// STRUCTURES

JSONArray data_properties;
HashMap<String, String> viz_properties_string = new HashMap<String, String>();
HashMap<String, Integer> viz_properties_integer = new HashMap<String, Integer>();
HashMap<String, Float> viz_properties_float = new HashMap<String, Float>();
HashMap<String, Integer> viz_properties_color = new HashMap<String, Integer>();
HashMap<String, String[]> viz_properties_stringArray = new HashMap<String, String[]>();
HashMap<String, HashMap<String, Integer>> viz_properties_HMcolor = new HashMap<String, HashMap<String, Integer>>();

// DATA

boolean FOLDER_IS_SELECTED = false;
String DATA_FOLDER_PATH;

String coord_file_name = "coord"; // name of the csv file containing the coordinates of the nodes without the extension (.csv)
Table TABLE_DATA_COORD;
String network_file_name = "network"; // name of the csv file containing the network data without the extension (.csv)
BufferedReader READER_DATA_NETWORK;
String THE_LINE_NET_DATA = null;
int number_of_lines = 0;

boolean NODE_ID_LOADED = false;
boolean LOADING_NODE_ID_LAUNCHED = false;
int index_node_id_loaded = 0;
int index_node_id_loaded_prev = 0;

boolean date_pbm_timestep = false;
boolean date_pbm_dateformat = false;

boolean pbm_coordinates_format = false;

boolean NODES_COORDINATES_RESCALED = false;
boolean RESCALE_NODES_COORDINATES_LAUNCHED = false;
int index_nodes_coordinates_rescaled = 0;

boolean NETWORK_DATA_LOADED = false;
boolean LOADING_NETWORK_DATA_LAUNCHED = false;
int index_network_data_loaded = 0;


// TEMPORAL SCALE AND FIRST DATE OF THE DATA
String temporal_scale = "month"; // year / month / week_num / seven_days / day / timestep

String FIRST_DATE_STR = "NA";
Date FIRST_DATE_SDF;
Calendar FIRST_DATE_CALENDAR = Calendar.getInstance();
int FIRST_DATE_DAY_OF_MONTH;
int FIRST_DATE_WEEK_OF_YEAR;
int FIRST_DATE_MONTH;
int FIRST_DATE_YEAR;


// NETWORK STRUCTURE

Set<String> list_id_of_all_nodes = new HashSet<String>();

HashMap<String, Node> hm_nodes = new HashMap<String, Node>();
Set<String> list_id_of_node_with_coordinates = new HashSet<String>();
Set<String> list_id_of_node_without_coordinates = new HashSet<String>();
float number_of_nodes;

int index_column_type = 0; // = 0
String[] flow_type = {"NA"};


// PARAMETERS RELATED TO THE COORDINATES PROJECTION

String coordinates_format = "gps"; // gps / projection

float max_data_longitude;
float min_data_longitude;
float max_data_latitude;
float min_data_latitude;

float min_proj_longitude;
float max_proj_longitude;
float min_proj_latitude;
float max_proj_latitude;

float mean_data_longitude;
float mean_data_latitude;

String boundaries_geojson_file = "NA";
JSONArray boundaries_geojson;
PShape areaBoundariesShape;
color col_boundaries_shape_fill = color(#E0E0E0);
color col_boundaries_shape_stroke = color(#D0D0D0);
float boundaries_shape_strokeWeight = 0.5;


//// COLORS

//color whiteColor = color(#FFFFFF); // blanc
//color blackColor = color(#0A0A0A); // black
//color lightGreyColor = color(#E6E6E6); // light grey
//color redColor = color(#800040); // rouge
//color blueColor = color(#006496); // bleu
//color greenColor = color(#00A480); // vert
//color orangeColor = color(#FF5308); // orange
//color purpleColor = color(#800080); // purple

color col_background = color(255);

// WINDOW MARGE

float margeX_init = 65;
float margeY_init = 65;

float margeX = margeX_init;
float margeY = margeY_init;


// PARAMETERS OF THE STATIC AND DYNAMIC VISUALIZATION

boolean NODES_ARE_DRAWN = true;
boolean LINKS_ARE_DRAWN = true;
boolean LINK_WITH_ARROW = false;
boolean LEGEND_IS_DRAWN = false;
boolean SAVE_PDF = false;
boolean SAVE_PNG = false;

boolean BUFFER_LINKS_ARE_DRAWN = true;
boolean REFRESH_VIZ = true;

boolean PNG_IMAGE_CROPPED = true;

boolean DYNAMIC_VIZ = false;
boolean SAVE_DYNAMIC_VIZ = false;

int current_timestep_in_dynamic_viz;
int last_timestep_in_dynamic_viz;
float mov_step_index_in_dynamic_viz = 1;
float total_number_of_step_in_one_mov_for_dynamic_viz = 40;
int amplitude_of_the_numer_of_frames_for_dynamic_viz = 5;
int frame_number_in_dynamic_viz = 1;

int min_timestep_viz = 1;
int max_timestep_viz = 1;
int timestep_start_viz = 1;
int timestep_end_viz = 1;


// BUTTON POSITION AND FUNCTION

color dynamic_viz_bar_color_highlight;
color dynamic_viz_bar_color_highlight_save;

int exit_button_pos_X, exit_button_pos_Y;      // Position of exit button
int exit_button_size = 9;
boolean mouseOverExitButton = false;


// FOCUS NODE

Node focus_node = null;


// FONT

PFont THE_FONT;


// NODE AND LINK APPEARANCE

float max_of_in_and_out_flow = 0;
float threshold_for_the_max_of_in_and_out_flow = 20000;

color col_node_outgoing_flow = color(#0A0A0A);
color col_node_ingoing_flow = color(#0A0A0A);
float node_transparency = 180; // from 0 to 255

//float node_diameter_min = 1.;
float node_diameter_max = 30.;
float focus_node_halo_size = 10.; // will be add to the focus node diameter
color col_focus_node_halo = color(#999999); // medium-light grey
float focus_node_halo_transparency = 100;

HashMap<String, Integer> col_link_flow = new HashMap<String, Integer>();
HashMap<String, Integer> col_link_outgoing_flow_for_focus_node = new HashMap<String, Integer>();
HashMap<String, Integer> col_link_ingoing_flow_for_focus_node = new HashMap<String, Integer>();

float threshold_min_flow_size_strokeWeight = 1;
float threshold_max_flow_size_strokeWeight = 500;
float threshold_link_strokeWeight_min = 0.25;
float threshold_link_strokeWeight_max = 8;

float threshold_min_flow_size_strokeTransparency = 1;
float threshold_max_flow_size_strokeTransparency = 500;
float threshold_link_strokeTransparency_min = 1;
float threshold_link_strokeTransparency_max = 200;
float threshold_link_strokeTransparency_min_for_focus_node = 50;
float threshold_link_strokeTransparency_max_for_focus_node = 200;

float threshold_min_flow_to_draw = 10;
float threshold_min_flow_to_draw_for_focus_node = 1;

HashMap<String, Integer> col_outgoing_particle_for_dynamic_viz = new HashMap<String, Integer>();
HashMap<String, Integer> col_ingoing_particle_for_dynamic_viz = new HashMap<String, Integer>();

float particle_color_transparency = 200;
float threshold_min_flow_to_draw_for_dynamic_viz = 1;
float threshold_max_flow_size_for_dynamic_viz = 50;
float particle_min_size_for_dynamic_viz = 1.5; // min diameter of the particle
float ratio_between_the_min_and_max_particle_area_for_dynamic_viz = 100; // ratio between the min and max particle area

float text_size_time_step = 15;

// time bar

float timebar_x_min;
float timebar_x_max;
float timebar_y;

float timebar_w = 8;
float timebar_hw = timebar_w/2.;

float timebar_x_start;
float timebar_x_end;

boolean current_time_bar_hold = false;
boolean current_time_bar_start_hold = false;
boolean current_time_bar_end_hold = false;


//      ___           ___                                               ___           ___           ___
//     /  /\         /  /\          ___         ___       ___          /__/\         /  /\         /  /\
//    /  /:/_       /  /:/_        /  /\       /  /\     /  /\         \  \:\       /  /:/_       /  /:/_
//   /  /:/ /\     /  /:/ /\      /  /:/      /  /:/    /  /:/          \  \:\     /  /:/ /\     /  /:/ /\
//  /  /:/ /::\   /  /:/ /:/_    /  /:/      /  /:/    /__/::\      _____\__\:\   /  /:/_/::\   /  /:/ /::\
// /__/:/ /:/\:\ /__/:/ /:/ /\  /  /::\     /  /::\    \__\/\:\__  /__/::::::::\ /__/:/__\/\:\ /__/:/ /:/\:\
// \  \:\/:/~/:/ \  \:\/:/ /:/ /__/:/\:\   /__/:/\:\      \  \:\/\ \  \:\~~\~~\/ \  \:\ /~~/:/ \  \:\/:/~/:/
//  \  \::/ /:/   \  \::/ /:/  \__\/  \:\  \__\/  \:\      \__\::/  \  \:\  ~~~   \  \:\  /:/   \  \::/ /:/
//   \__\/ /:/     \  \:\/:/        \  \:\      \  \:\     /__/:/    \  \:\        \  \:\/:/     \__\/ /:/
//     /__/:/       \  \::/          \__\/       \__\/     \__\/      \  \:\        \  \::/        /__/:/
//     \__\/         \__\/                                             \__\/         \__\/         \__\/

//void settings() {
//  //pixelDensity(displayDensity());
//  //size(displayWidth-100, displayHeight-50, JAVA2D);
//  fullScreen(JAVA2D, 1);
//  println("width: ", width);
//}


//      ___           ___                       ___           ___
//     /  /\         /  /\          ___        /__/\         /  /\
//    /  /:/_       /  /:/_        /  /\       \  \:\       /  /::\
//   /  /:/ /\     /  /:/ /\      /  /:/        \  \:\     /  /:/\:\
//  /  /:/ /::\   /  /:/ /:/_    /  /:/     ___  \  \:\   /  /:/~/:/
// /__/:/ /:/\:\ /__/:/ /:/ /\  /  /::\    /__/\  \__\:\ /__/:/ /:/
// \  \:\/:/~/:/ \  \:\/:/ /:/ /__/:/\:\   \  \:\ /  /:/ \  \:\/:/
//  \  \::/ /:/   \  \::/ /:/  \__\/  \:\   \  \:\  /:/   \  \::/
//   \__\/ /:/     \  \:\/:/        \  \:\   \  \:\/:/     \  \:\
//     /__/:/       \  \::/          \__\/    \  \::/       \  \:\
//     \__\/         \__\/                     \__\/         \__\/

void setup() {
  // display settings
  //pixelDensity(displayDensity());
  //size(displayWidth-100, displayHeight-50, JAVA2D);
  fullScreen(JAVA2D, 1);
  println("width: ", width);
  // general appearance
  smooth();
  background(255);
  frameRate(30);
  strokeCap(SQUARE);
  cursor(HAND);
  // load the font
  //THE_FONT = createFont("ProcessingSansPro-Regular", 15); // THE_FONT = createFont("SansSerif", 15);
  THE_FONT = createFont("HelveticaNeue", 15);
  // buttons characteristics
  // exit button
  exit_button_pos_X = 15; // 25; // 10;
  exit_button_pos_Y = 15; // height - 30; // 10;
  // time bar
  dynamic_viz_bar_color_highlight = color(50);
  dynamic_viz_bar_color_highlight_save = color(#800040);
  timebar_x_min = 50.;
  timebar_x_max = width-50.;
  timebar_y = height-(margeY_init)/2.;
  timebar_x_start = timebar_x_min;
  timebar_x_end = timebar_x_max;
  // set the title of the window
  surface.setTitle("GeoGraph-Viz: visualization of network data considering geographic information");
  // init hashmap of parameters
  viz_properties_string.put("coord_file_name", coord_file_name);
  viz_properties_string.put("network_file_name", network_file_name);
  viz_properties_string.put("temporal_scale", temporal_scale);
  viz_properties_string.put("coordinates_format", coordinates_format);
  viz_properties_string.put("boundaries_geojson_file", boundaries_geojson_file);
  //
  viz_properties_integer.put("index_column_type", index_column_type);
  //
  viz_properties_float.put("total_number_of_step_in_one_mov_for_dynamic_viz", total_number_of_step_in_one_mov_for_dynamic_viz);
  viz_properties_float.put("threshold_for_the_max_of_in_and_out_flow", threshold_for_the_max_of_in_and_out_flow);
  viz_properties_float.put("node_transparency", node_transparency);
  viz_properties_float.put("node_diameter_max", node_diameter_max);
  viz_properties_float.put("focus_node_halo_size", focus_node_halo_size);
  viz_properties_float.put("focus_node_halo_transparency", focus_node_halo_transparency);
  viz_properties_float.put("threshold_min_flow_size_strokeWeight", threshold_min_flow_size_strokeWeight);
  viz_properties_float.put("threshold_max_flow_size_strokeWeight", threshold_max_flow_size_strokeWeight);
  viz_properties_float.put("threshold_link_strokeWeight_min", threshold_link_strokeWeight_min);
  viz_properties_float.put("threshold_link_strokeWeight_max", threshold_link_strokeWeight_max);
  viz_properties_float.put("threshold_min_flow_size_strokeTransparency", threshold_min_flow_size_strokeTransparency);
  viz_properties_float.put("threshold_max_flow_size_strokeTransparency", threshold_max_flow_size_strokeTransparency);
  viz_properties_float.put("threshold_link_strokeTransparency_min", threshold_link_strokeTransparency_min);
  viz_properties_float.put("threshold_link_strokeTransparency_max", threshold_link_strokeTransparency_max);
  viz_properties_float.put("threshold_link_strokeTransparency_min_for_focus_node", threshold_link_strokeTransparency_min_for_focus_node);
  viz_properties_float.put("threshold_link_strokeTransparency_max_for_focus_node", threshold_link_strokeTransparency_max_for_focus_node);
  viz_properties_float.put("threshold_min_flow_to_draw", threshold_min_flow_to_draw);
  viz_properties_float.put("threshold_min_flow_to_draw_for_focus_node", threshold_min_flow_to_draw_for_focus_node);
  viz_properties_float.put("particle_color_transparency", particle_color_transparency);
  viz_properties_float.put("threshold_min_flow_to_draw_for_dynamic_viz", threshold_min_flow_to_draw_for_dynamic_viz);
  viz_properties_float.put("threshold_max_flow_size_for_dynamic_viz", threshold_max_flow_size_for_dynamic_viz);
  viz_properties_float.put("particle_min_size_for_dynamic_viz", particle_min_size_for_dynamic_viz);
  viz_properties_float.put("ratio_between_the_min_and_max_particle_area_for_dynamic_viz", ratio_between_the_min_and_max_particle_area_for_dynamic_viz);
  viz_properties_float.put("text_size_time_step", text_size_time_step);
  viz_properties_float.put("boundaries_shape_strokeWeight", boundaries_shape_strokeWeight);
  //
  viz_properties_color.put("col_background", col_background);
  viz_properties_color.put("col_node_outgoing_flow", col_node_outgoing_flow);
  viz_properties_color.put("col_node_ingoing_flow", col_node_ingoing_flow);
  viz_properties_color.put("col_focus_node_halo", col_focus_node_halo);
  viz_properties_color.put("col_boundaries_shape_fill", col_boundaries_shape_fill);
  viz_properties_color.put("col_boundaries_shape_stroke", col_boundaries_shape_stroke);
  //
  viz_properties_stringArray.put("flow_type", flow_type);
  //
  col_link_flow.put("NA", #1E1E1E);
  viz_properties_HMcolor.put("col_link_flow", col_link_flow);
  col_link_outgoing_flow_for_focus_node.put("NA", #0A0A0A);
  viz_properties_HMcolor.put("col_link_outgoing_flow_for_focus_node", col_link_outgoing_flow_for_focus_node);
  col_link_ingoing_flow_for_focus_node.put("NA", #0A0A0A);
  viz_properties_HMcolor.put("col_link_ingoing_flow_for_focus_node", col_link_ingoing_flow_for_focus_node);
  col_outgoing_particle_for_dynamic_viz.put("NA", #0A0A0A);
  viz_properties_HMcolor.put("col_outgoing_particle_for_dynamic_viz", col_outgoing_particle_for_dynamic_viz);
  col_ingoing_particle_for_dynamic_viz.put("NA", #0A0A0A);
  viz_properties_HMcolor.put("col_ingoing_particle_for_dynamic_viz", col_ingoing_particle_for_dynamic_viz);
  
  //
  areaBoundariesShape = createShape(GROUP);

//  // data loading
//  TABLE_DATA_COORD = loadTable(DATA_FOLDER_PATH + "/" + viz_properties_string["coord_file_name"] + ".csv", "header, csv");
//  //////////////////////////////////////////////////////////////////////////////////////
//  // IF MULTI-THREAD NOT AVAILABLE
//  // Open a first reader on the network data file in order to compute the number of line to read and find the first date
//  loadNodeIDs();
//  // Rescale the node coordinates in order to fit to the window size and keep the right proportion
//  rescaleNodesCoordinates();
//  // Open a second reader on the network data file in order to load the data and compute information on edges
//  loadNetworkData();
//  //////////////////////////////////////////////////////////////////////////////////////
}


//     _____          ___           ___           ___
//    /  /::\        /  /\         /  /\         /__/\
//   /  /:/\:\      /  /::\       /  /::\       _\_ \:\
//  /  /:/  \:\    /  /:/\:\     /  /:/\:\     /__/\ \:\
// /__/:/ \__\:|  /  /:/~/:/    /  /:/~/::\   _\_ \:\ \:\
// \  \:\ /  /:/ /__/:/ /:/___ /__/:/ /:/\:\ /__/\ \:\ \:\
//  \  \:\  /:/  \  \:\/:::::/ \  \:\/:/__\/ \  \:\ \:\/:/
//   \  \:\/:/    \  \::/~~~~   \  \::/       \  \:\ \::/
//    \  \::/      \  \:\        \  \:\        \  \:\/:/
//     \__\/        \  \:\        \  \:\        \  \::/
//                   \__\/         \__\/         \__\/

void draw() {
  strokeCap(ROUND);
  cursor(HAND);

  textFont(THE_FONT, 12);

  if (!FOLDER_IS_SELECTED) {
    displayKeyboardShortcuts();
    noLoop();
    //selectFolder("Select the folder containing the data (network + coordinates):", "folderSelected");
  } else {
    if (NODE_ID_LOADED == false) {
      if (date_pbm_timestep | date_pbm_dateformat) {
        delay(10*1000);
        exit();
      }
      if (LOADING_NODE_ID_LAUNCHED == false) {
        thread("loadNodeIDs");
        LOADING_NODE_ID_LAUNCHED = true;
      }
      // update the loading information
      background(255);
      fill(10);
      textAlign(CENTER,CENTER);
      //text("loading data",width/2,height/2);
      text("index node ID: ",width/2,height/2);
      text(index_node_id_loaded,width/2,height/2+20);
      if (date_pbm_timestep) {
        fill(#800040);
        textAlign(CENTER,CENTER);
        text("Error: the provided dates do not seem to be in time-steps format.",width/2,height/2+60);
        text("This program will automatically stop in 10 seconds.",width/2,height/2+80);
      }
      if (date_pbm_dateformat) {
        fill(#800040);
        textAlign(CENTER,CENTER);
        text("Error: the provided dates do not seem to be in date format ISO 8601 (YYYY-MM-DD).",width/2,height/2+60);
        text("This program will automatically stop in 10 seconds.",width/2,height/2+80);
      }
    } else
    if (NODES_COORDINATES_RESCALED == false) {
      // print results of previous step
      println("FIRST_DATE_STR:", FIRST_DATE_STR);
      if (viz_properties_string.get("temporal_scale").equals("timestep") == false) {
        try {
          FIRST_DATE_SDF = new SimpleDateFormat("yyyy-MM-dd").parse(FIRST_DATE_STR);
        }
          catch (Exception e) {
        }
        FIRST_DATE_CALENDAR.setMinimalDaysInFirstWeek(1);
        FIRST_DATE_CALENDAR.setTime(FIRST_DATE_SDF);
        FIRST_DATE_DAY_OF_MONTH = FIRST_DATE_CALENDAR.get(Calendar.DAY_OF_MONTH);
        FIRST_DATE_WEEK_OF_YEAR = FIRST_DATE_CALENDAR.get(Calendar.WEEK_OF_YEAR);
        FIRST_DATE_MONTH = FIRST_DATE_CALENDAR.get(Calendar.MONTH)+1;
        FIRST_DATE_YEAR = FIRST_DATE_CALENDAR.get(Calendar.YEAR);
      }
      if (RESCALE_NODES_COORDINATES_LAUNCHED == false) {
        thread("rescaleNodesCoordinates");
        RESCALE_NODES_COORDINATES_LAUNCHED = true;
      }
      // update the loading bar
      background(255);
      fill(10);
      if (index_nodes_coordinates_rescaled < list_id_of_all_nodes.size()) {
        textAlign(CENTER,CENTER);
        //text("loading data",width/2,height/2);
        text("rescale nodes coordinates: " + nf(round(float(index_nodes_coordinates_rescaled)/float(list_id_of_all_nodes.size())*100.),0,0) + " %",width/2,height/2);
        strokeWeight(4);
        stroke(230,200);
        line(width/2-100,height/2+20,width/2+100,height/2+20);
        stroke(30,200);
        line(width/2-100,height/2+20,width/2-100+2*(float(index_nodes_coordinates_rescaled)/float(list_id_of_all_nodes.size())*100.),height/2+20);
      } else if (index_nodes_coordinates_rescaled >= list_id_of_all_nodes.size()) {
        NODES_COORDINATES_RESCALED = true;
        textAlign(CENTER,CENTER);
        //text("loading data",width/2,height/2);
        text("rescale nodes coordinates: " + nf(round(float(index_nodes_coordinates_rescaled)/float(list_id_of_all_nodes.size())*100.),0,0) + " %",width/2,height/2);
        strokeWeight(4);
        stroke(230,200);
        line(width/2-100,height/2+20,width/2+100,height/2+20);
        stroke(30,200);
        line(width/2-100,height/2+20,width/2-100+2*(float(index_nodes_coordinates_rescaled)/float(list_id_of_all_nodes.size())*100.),height/2+20);
        // TODO: implement a loading section for the area boundaries after windows size and boundaries were defined
        loadBoundariesGeojsonData();
      }
    } else
    if (NETWORK_DATA_LOADED == false) {
      if (LOADING_NETWORK_DATA_LAUNCHED == false) {
        thread("loadNetworkData");
        LOADING_NETWORK_DATA_LAUNCHED = true;
      }
      // update the loading bar
      background(255);
      fill(10);
      if (index_network_data_loaded < number_of_lines) {
        textAlign(CENTER,CENTER);
        //text("loading data",width/2,height/2);
        text("loading network data: " + nf(round(float(index_network_data_loaded)/float(number_of_lines)*100.),0,0) + " %",width/2,height/2);
        strokeWeight(4);
        stroke(230,200);
        line(width/2-100,height/2+20,width/2+100,height/2+20);
        stroke(30,200);
        line(width/2-100,height/2+20,width/2-100+2*(float(index_network_data_loaded)/float(number_of_lines)*100.),height/2+20);
      } else
      if (index_network_data_loaded == number_of_lines) {
        NETWORK_DATA_LOADED = true;
        // update the loading information
        textAlign(CENTER,CENTER);
        text("creating visualization ...",width/2,height/2);
        //
        timestep_start_viz = min_timestep_viz;
        timestep_end_viz = max_timestep_viz;
      }
    } else {
      if (SAVE_PDF) {
        beginRecord(PDF, DATA_FOLDER_PATH + "/figures/" + viz_properties_string.get("network_file_name") + ".pdf");
      }
      //
      networkDrawing ();
      //
      if (SAVE_PDF) {
        endRecord();
        SAVE_PDF = false;
        REFRESH_VIZ = false;
      }
      //
      if (SAVE_PNG) {
        println("saving png!");
        if (PNG_IMAGE_CROPPED) {
          PImage img;
          img = get(round(margeX-margeX_init), round(margeY-margeY_init), round(width - 2*round(margeX-margeX_init)), round(height - 2*round(margeY-margeY_init)));
          img.save(DATA_FOLDER_PATH + "/figures/" + viz_properties_string.get("network_file_name") + "_cropped.png");
        } else {
          saveFrame(DATA_FOLDER_PATH + "/figures/" + viz_properties_string.get("network_file_name") + ".png");
        }
        SAVE_PNG = false;
        REFRESH_VIZ = false;
      }
      // get position of the mouse and check if over something
      update_x_and_y_position();
      // exit/quit button
      fill(255);
      stroke(255);
      strokeWeight(3.);
      rect(exit_button_pos_X, exit_button_pos_Y, exit_button_size, exit_button_size);
      //rect(exit_button_pos_X-exit_button_size/2., exit_button_pos_Y-exit_button_size/2., exit_button_size*2., exit_button_size*2.);
      if (mouseOverExitButton == true) {
        noFill();
        stroke(#800040);
      } else {
        noFill();
        stroke(150);
      }
      strokeWeight(1.25);
      rect(exit_button_pos_X, exit_button_pos_Y, exit_button_size, exit_button_size);
      float cross_shift = 2.;
      line(exit_button_pos_X+cross_shift, exit_button_pos_Y+cross_shift, exit_button_pos_X+exit_button_size-cross_shift, exit_button_pos_Y+exit_button_size-cross_shift);
      line(exit_button_pos_X+cross_shift, exit_button_pos_Y+exit_button_size-cross_shift, exit_button_pos_X+exit_button_size-cross_shift, exit_button_pos_Y+cross_shift);

      //
      timeBar();
      //
    }
  }
}


//      ___           ___           ___           ___           ___
//     /__/\         /  /\         /__/\         /  /\         /  /\
//    |  |::\       /  /::\        \  \:\       /  /:/_       /  /:/_
//    |  |:|:\     /  /:/\:\        \  \:\     /  /:/ /\     /  /:/ /\
//  __|__|:|\:\   /  /:/  \:\   ___  \  \:\   /  /:/ /::\   /  /:/ /:/_
// /__/::::| \:\ /__/:/ \__\:\ /__/\  \__\:\ /__/:/ /:/\:\ /__/:/ /:/ /\
// \  \:\~~\__\/ \  \:\ /  /:/ \  \:\ /  /:/ \  \:\/:/~/:/ \  \:\/:/ /:/
//  \  \:\        \  \:\  /:/   \  \:\  /:/   \  \::/ /:/   \  \::/ /:/
//   \  \:\        \  \:\/:/     \  \:\/:/     \__\/ /:/     \  \:\/:/
//    \  \:\        \  \::/       \  \::/        /__/:/       \  \::/
//     \__\/         \__\/         \__\/         \__\/         \__\/

void mousePressed() {
  // select a node
  Node selectedNode = null;
  for (String id : list_id_of_node_with_coordinates) {
    if (NODES_ARE_DRAWN) {
      Node checkNode = hm_nodes.get(id);
      float d = dist(mouseX, mouseY, checkNode.x, checkNode.y);
      float total_flow = checkNode.ingoing_flow + checkNode.outgoing_flow + checkNode.ingoing_flow_from_na + checkNode.outgoing_flow_to_na;
      //float diam = 2.*sqrt(((min(total_flow,max_of_in_and_out_flow)/max_of_in_and_out_flow) * (sq(viz_properties_float.get("node_diameter_max")/2.)*PI))/PI);
      float diam = 2.*sqrt((min(total_flow,max_of_in_and_out_flow)/max_of_in_and_out_flow) * sq(viz_properties_float.get("node_diameter_max")/2.));
      //diam = max(diam, 1.);
      float circleLimit = diam/2.;
      if (d < circleLimit) {
        selectedNode = checkNode;
        break;
      }
    }
  }
  if (selectedNode != null) {
    if (focus_node == null) {
      BUFFER_LINKS_ARE_DRAWN = LINKS_ARE_DRAWN;
    }
    focus_node = selectedNode;
    LINKS_ARE_DRAWN = false;
    REFRESH_VIZ = true;
  } else {
    if (mouseY < height-35) {
      if (focus_node != null) {
        focus_node = null;
        REFRESH_VIZ = true;
        LINKS_ARE_DRAWN = BUFFER_LINKS_ARE_DRAWN;
      }
    }
  }
  // quit
  if ( (mouseX >= exit_button_pos_X) && (mouseX <= exit_button_pos_X+exit_button_size) && (mouseY >= exit_button_pos_Y) && ( mouseY <= exit_button_pos_Y+exit_button_size) ) {
    exit();
  }
}


void mouseReleased() {
  if (RANGE_WAS_UPDATED) {
    REFRESH_VIZ = true;
    RANGE_WAS_UPDATED = false;
  }
  current_time_bar_hold = false;
  current_time_bar_start_hold = false;
  current_time_bar_end_hold = false;
}


//      ___           ___
//     /__/|         /  /\          ___
//    |  |:|        /  /:/_        /__/|
//    |  |:|       /  /:/ /\      |  |:|
//  __|  |:|      /  /:/ /:/_     |  |:|
// /__/\_|:|____ /__/:/ /:/ /\  __|__|:|
// \  \:\/:::::/ \  \:\/:/ /:/ /__/::::\
//  \  \::/~~~~   \  \::/ /:/     ~\~~\:\
//   \  \:\        \  \:\/:/        \  \:\
//    \  \:\        \  \::/          \__\/
//     \__\/         \__\/

void keyPressed() {
  // display / hide nodes
  if (key=='n' || key=='N') {
    NODES_ARE_DRAWN = !NODES_ARE_DRAWN;
    REFRESH_VIZ = true;
  }
  // display / hide edges
  if (key=='e' || key=='E') {
    LINKS_ARE_DRAWN = !LINKS_ARE_DRAWN;
    REFRESH_VIZ = true;
    focus_node = null;
  }
  // display / hide the legend informations
  if (key=='l' || key=='L') {
    LEGEND_IS_DRAWN = !LEGEND_IS_DRAWN;
    REFRESH_VIZ = true;
  }
  // switch between lines and arrows for the representation of edges
  if (key=='a' || key=='A') {
    LINK_WITH_ARROW = !LINK_WITH_ARROW;
    REFRESH_VIZ = true;
  }
  // save the current visualization as PDF file
  if (key=='v' || key=='V') {
    SAVE_PDF = !SAVE_PDF;
    REFRESH_VIZ = true;
  }
  // save the current visualization as PNG file
  if (key=='p' || key=='P') {
    SAVE_PNG = !SAVE_PNG;
    REFRESH_VIZ = true;
  }
  // activate/desactivate image cropping (PNG file only)
  if (key=='c' || key=='C') {
    PNG_IMAGE_CROPPED = !PNG_IMAGE_CROPPED;
  }
  // refresh the viz
  if (key=='r' || key=='R') {
    refresh_viz_properties();
    areaBoundariesShape = createShape(GROUP);
    loadBoundariesGeojsonData();
    REFRESH_VIZ = true; // REFRESH_VIZ = !REFRESH_VIZ;
  }
  // start/stop the dynamic visualization
  if (key=='d' || key=='D') {
    if (DYNAMIC_VIZ == false) {
      DYNAMIC_VIZ = true;
      current_timestep_in_dynamic_viz = timestep_start_viz;
      last_timestep_in_dynamic_viz = timestep_end_viz;
      int total_number_of_frames_for_dynamic_viz = int(viz_properties_float.get("total_number_of_step_in_one_mov_for_dynamic_viz") * (timestep_end_viz - timestep_start_viz + 1));
      amplitude_of_the_numer_of_frames_for_dynamic_viz = str(total_number_of_frames_for_dynamic_viz).length();
      frameCount = 0;
      frame_number_in_dynamic_viz = 1;
      mov_step_index_in_dynamic_viz = 1;
      REFRESH_VIZ = true;
      focus_node = null;
    } else {
      DYNAMIC_VIZ = false;
      REFRESH_VIZ = true;
      LINKS_ARE_DRAWN = true;
    }
  }
  // activate/desactivate the record of dynamic viz
  if (key=='s' || key=='S') {
    SAVE_DYNAMIC_VIZ = !SAVE_DYNAMIC_VIZ;
  }
  // quit
  if (key=='q' || key=='Q') {
    exit();
  }
  // select / change the data folder
  if (key == 'o' || key == 'O') {
    noLoop();
    selectFolder("Select the folder containing the data (network + coordinates + configuration file):", "folderSelected");
  }
}


void displayKeyboardShortcuts() {

  float theTextFontSize = 12;
  float theTextLeading = theTextFontSize + 0;
  textLeading(theTextLeading);
  float theMarge = theTextFontSize*2;
  textSize(theTextFontSize);
  float horizontalSpace = 30.0;

  //String KeyboardShortcuts_part1 = "o / O"
  //+ "\nq / Q"
  //+ "\n"
  //+ "\nn / N"
  //+ "\ne / E"
  //+ "\np / P"
  //+ "\nc / C"
  //+ "\nv / V"
  //+ "\nr / R"
  //+ "\nd / D"
  //+ "\ns / S"
  //+ "\n"
  //+ "\na / A"
  //+ "\nl / L";

  String KeyboardShortcuts_part1 = "O"
  + "\nQ"
  + "\n"
  + "\nN"
  + "\nE"
  + "\nP"
  + "\nC"
  + "\nV"
  + "\nR"
  + "\nD"
  + "\nS"
  + "\n"
  + "\nA"
  + "\nL";

  String KeyboardShortcuts_part2 = "-"
  + "\n-"
  + "\n"
  + "\n-"
  + "\n-"
  + "\n-"
  + "\n-"
  + "\n-"
  + "\n-"
  + "\n-"
  + "\n-"
  + "\n"
  + "\n-"
  + "\n-";

  String KeyboardShortcuts_part3 = "select / change the data folder (what you need to do to start)"
  + "\nquit the program"
  + "\n"
  + "\ndisplay / hide nodes"
  + "\ndisplay / hide edges"
  + "\nsave the current visualization as PNG file"
  + "\nactivate/desactivate image cropping (PNG file only)"
  + "\nsave the current visualization as vector graphics (PDF file)"
  + "\nrefresh the visualization"
  + "\nstart/stop the dynamic visualization"
  + "\nactivate/desactivate the record of dynamic viz (as frame images), Then you can"
  + "\nmake a video from this set of images using the Processing Movie Maker tool."
  + "\nswitch between lines and arrows for the representation of edges"
  + "\ndisplay / hide the legend informations";

  float theTextWidth = textWidth(KeyboardShortcuts_part1) + textWidth(KeyboardShortcuts_part3) + horizontalSpace;
  float theLineHeight = theTextLeading + (textAscent() + textDescent())*0.7;
  float theTextHeight = 16 * theLineHeight;

  noStroke();
  // draw text background
  fill(255, 240);
  rect((width-theTextWidth)/2.-theMarge, (height-theTextHeight)/2.-theMarge,
       theTextWidth+2*theMarge, theTextHeight+2*theMarge);
  // print text
  fill(10);
  textAlign(LEFT, TOP);
  text("Keyboard shortcuts (not case sensitive)", (width-theTextWidth)/2., (height-theTextHeight)/2.);
  textAlign(RIGHT, TOP);
  textAlign(CENTER, TOP);
  text(KeyboardShortcuts_part1, (width-theTextWidth)/2.+textWidth(KeyboardShortcuts_part1), (height-theTextHeight)/2.+2*theLineHeight);
  textAlign(CENTER, TOP);
  text(KeyboardShortcuts_part2, (width-theTextWidth)/2.+textWidth(KeyboardShortcuts_part1)+horizontalSpace/2., (height-theTextHeight)/2.+2*theLineHeight);
  textAlign(LEFT, TOP);
  text(KeyboardShortcuts_part3, (width-theTextWidth)/2.+textWidth(KeyboardShortcuts_part1)+horizontalSpace, (height-theTextHeight)/2.+2*theLineHeight);

  //
  noLoop();
}


//                              ___           ___
//      ___       ___          /__/\         /  /\
//     /  /\     /  /\        |  |::\       /  /:/_
//    /  /:/    /  /:/        |  |:|:\     /  /:/ /\
//   /  /:/    /__/::\      __|__|:|\:\   /  /:/ /:/_
//  /  /::\    \__\/\:\__  /__/::::| \:\ /__/:/ /:/ /\
// /__/:/\:\      \  \:\/\ \  \:\~~\__\/ \  \:\/:/ /:/
// \__\/  \:\      \__\::/  \  \:\        \  \::/ /:/
//      \  \:\     /__/:/    \  \:\        \  \:\/:/
//       \__\/     \__\/      \  \:\        \  \::/
//                             \__\/         \__\/
//                    ___           ___
//     _____         /  /\         /  /\
//    /  /::\       /  /::\       /  /::\
//   /  /:/\:\     /  /:/\:\     /  /:/\:\
//  /  /:/~/::\   /  /:/~/::\   /  /:/~/:/
// /__/:/ /:/\:| /__/:/ /:/\:\ /__/:/ /:/___
// \  \:\/:/~/:/ \  \:\/:/__\/ \  \:\/:::::/
//  \  \::/ /:/   \  \::/       \  \::/~~~~
//   \  \:\/:/     \  \:\        \  \:\
//    \  \::/       \  \:\        \  \:\
//     \__\/         \__\/         \__\/

void timeBar() {
  noFill();
  strokeCap(ROUND);

  // cover the previous bar
  stroke(255);
  fill(255);
  //strokeWeight(timebar_w+2);
  //line(timebar_x_min, timebar_y, timebar_x_max, timebar_y);
  noStroke();
  rect(0, timebar_y-(margeY_init)/2., width, timebar_y+(margeY_init)/2.);

  // draw a new bar
  strokeWeight(timebar_w);

  // initialize a new bar
  stroke(230);
  line(timebar_x_min, timebar_y, timebar_x_max, timebar_y);

  // period displayed
  stroke(150);
  if ( (mouseX >= timebar_x_start+timebar_hw) && (mouseX <= timebar_x_end-timebar_hw) && (mouseY >= timebar_y-timebar_hw) && ( mouseY <= timebar_y+timebar_hw) & !DYNAMIC_VIZ ) {
    if (!current_time_bar_start_hold & !current_time_bar_end_hold) {
      stroke(50);
      if (mousePressed == true) {
        current_time_bar_hold = true;
      }
    }
  }
  if (current_time_bar_hold) {
    stroke(50);
    float shift = mouseX - pmouseX;
    float new_timebar_x_start = timebar_x_start + shift;
    if (new_timebar_x_start < timebar_x_min) {
      shift += (timebar_x_min - new_timebar_x_start);
    }
    float new_timebar_x_end = timebar_x_end + shift;
    if (new_timebar_x_end > timebar_x_max) {
      shift += (timebar_x_max - new_timebar_x_end);
    }
    timebar_x_start += shift;
    timebar_x_end += shift;
    RANGE_WAS_UPDATED = true;
  }
  line(timebar_x_start, timebar_y, timebar_x_end, timebar_y);


  // cursors - start
  if ( (mouseX >= timebar_x_start-timebar_hw) && (mouseX <= timebar_x_start+timebar_hw) && (mouseY >= timebar_y-timebar_hw) && ( mouseY <= timebar_y+timebar_hw) & !DYNAMIC_VIZ ) {
    stroke(50);
    line(timebar_x_start, timebar_y, timebar_x_start, timebar_y);
    if (mousePressed == true & !current_time_bar_hold & !current_time_bar_end_hold) {
      current_time_bar_start_hold = true;
    }
  }
  if (current_time_bar_start_hold) {
    stroke(50);
    float shift = mouseX - pmouseX;
    float new_timebar_x_start = timebar_x_start + shift;
    if (new_timebar_x_start < timebar_x_min) {
      shift += (timebar_x_min - new_timebar_x_start);
    }
    timebar_x_start += shift;
    if (new_timebar_x_start >= timebar_x_end - 1) {
      timebar_x_start = timebar_x_end - 1;
    }
    line(timebar_x_start, timebar_y, timebar_x_start, timebar_y);
    RANGE_WAS_UPDATED = true;
  }

  // cursors end
  if ( (mouseX >= timebar_x_end-timebar_hw) && (mouseX <= timebar_x_end+timebar_hw) && (mouseY >= timebar_y-timebar_hw) && ( mouseY <= timebar_y+timebar_hw) & !DYNAMIC_VIZ ) {
    stroke(50);
    line(timebar_x_end, timebar_y, timebar_x_end, timebar_y);
    if (mousePressed == true & !current_time_bar_hold & !current_time_bar_start_hold) {
      current_time_bar_end_hold = true;
    }
  }
  if (current_time_bar_end_hold) {
    stroke(50);
    float shift = mouseX - pmouseX;
    float new_timebar_x_end = timebar_x_end + shift;
    if (new_timebar_x_end > timebar_x_max) {
      shift += (timebar_x_max - new_timebar_x_end);
    }
    timebar_x_end += shift;
    if (new_timebar_x_end <= timebar_x_start + 1) {
      timebar_x_end = timebar_x_start + 1;
    }
    line(timebar_x_end, timebar_y, timebar_x_end, timebar_y);
    RANGE_WAS_UPDATED = true;
  }

  // update start and end timestep of the visualization
  timestep_start_viz = round(map(timebar_x_start, timebar_x_min, timebar_x_max, min_timestep_viz, max_timestep_viz));
  timestep_end_viz = round(map(timebar_x_end, timebar_x_min, timebar_x_max, min_timestep_viz, max_timestep_viz));

  // animate the bar during dynamic visualization
  float timebar_x_current = timebar_x_start;
  if (DYNAMIC_VIZ) {
    stroke(dynamic_viz_bar_color_highlight);
    if (SAVE_DYNAMIC_VIZ) {
      stroke(dynamic_viz_bar_color_highlight_save);
    }
    float progress = (current_timestep_in_dynamic_viz - timestep_start_viz) * total_number_of_step_in_one_mov_for_dynamic_viz + mov_step_index_in_dynamic_viz;
    progress = progress/ ((timestep_end_viz-timestep_start_viz+1) * total_number_of_step_in_one_mov_for_dynamic_viz);
    timebar_x_current = lerp(timebar_x_start, timebar_x_end, progress);
    line(timebar_x_start, timebar_y, timebar_x_current, timebar_y);
  }

  // text
  textFont(THE_FONT, 12);
  if ( ((mouseX >= timebar_x_min-timebar_hw) && (mouseX <= timebar_x_max+timebar_hw) && (mouseY >= timebar_y-timebar_hw) && ( mouseY <= timebar_y+timebar_hw))
       | current_time_bar_hold | current_time_bar_start_hold | current_time_bar_end_hold ) {
    // min and max
    fill(150);
    textAlign(CENTER, TOP);
    text(min_timestep_viz, timebar_x_min, timebar_y+timebar_hw + 2);
    text(max_timestep_viz, timebar_x_max, timebar_y+timebar_hw + 2);
    // start and end of the displayed period
    if (!DYNAMIC_VIZ) {
      fill(50);
      textAlign(CENTER, BOTTOM);
      text(timestep_start_viz, timebar_x_start, timebar_y-timebar_hw - 2);
      text(timestep_end_viz, timebar_x_end, timebar_y-timebar_hw - 2);
    } else {
      fill(50);
      textAlign(CENTER, BOTTOM);
      text(current_timestep_in_dynamic_viz, timebar_x_current, timebar_y-timebar_hw - 2);
    }
  }
}


//      ___                       ___           ___           ___
//     /  /\          ___        /__/\         /  /\         /  /\
//    /  /::\        /  /\       \  \:\       /  /:/_       /  /::\
//   /  /:/\:\      /  /:/        \__\:\     /  /:/ /\     /  /:/\:\
//  /  /:/  \:\    /  /:/     ___ /  /::\   /  /:/ /:/_   /  /:/~/:/
// /__/:/ \__\:\  /  /::\    /__/\  /:/\:\ /__/:/ /:/ /\ /__/:/ /:/___
// \  \:\ /  /:/ /__/:/\:\   \  \:\/:/__\/ \  \:\/:/ /:/ \  \:\/:::::/
//  \  \:\  /:/  \__\/  \:\   \  \::/       \  \::/ /:/   \  \::/~~~~
//   \  \:\/:/        \  \:\   \  \:\        \  \:\/:/     \  \:\
//    \  \::/          \__\/    \  \:\        \  \::/       \  \:\
//     \__\/                     \__\/         \__\/         \__\/

void folderSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");

    loop();
  } else {
    // set the data folder path
    DATA_FOLDER_PATH = selection.getAbsolutePath();
    println("User selected " + DATA_FOLDER_PATH);
    FOLDER_IS_SELECTED = true;
    //
    boundaries_geojson_file = "NA";
    viz_properties_string.put("boundaries_geojson_file", boundaries_geojson_file);
    areaBoundariesShape = createShape(GROUP);
    viz_properties_color.put("col_background", col_background);
    // properties loading
    refresh_viz_properties();
    // data loading
    TABLE_DATA_COORD = loadTable(DATA_FOLDER_PATH + "/" + viz_properties_string.get("coord_file_name") + ".csv", "header, csv");
    // RESET SOME VARIABLES
    // DATA
    number_of_lines = 0;
    NODE_ID_LOADED = false;
    LOADING_NODE_ID_LAUNCHED = false;
    index_node_id_loaded = 0;
    index_node_id_loaded_prev = 0;
    NODES_COORDINATES_RESCALED = false;
    RESCALE_NODES_COORDINATES_LAUNCHED = false;
    index_nodes_coordinates_rescaled = 0;
    NETWORK_DATA_LOADED = false;
    LOADING_NETWORK_DATA_LAUNCHED = false;
    index_network_data_loaded = 0;
    // TEMPORAL SCALE AND FIRST DATE OF THE DATA
    FIRST_DATE_STR = "NA";
    // NETWORK STRUCTURE
    list_id_of_all_nodes = new HashSet<String>();
    hm_nodes = new HashMap<String, Node>();
    list_id_of_node_with_coordinates = new HashSet<String>();
    list_id_of_node_without_coordinates = new HashSet<String>();
    number_of_nodes = 0;
    // PARAMETERS RELATED TO THE COORDINATES PROJECTION
    TableRow first_row = TABLE_DATA_COORD.getRow(0);
    float data_longitude = first_row.getFloat("coord_longitude");
    float data_latitude = first_row.getFloat("coord_latitude");
    max_data_longitude = data_longitude;
    min_data_longitude = data_longitude;
    max_data_latitude = data_latitude;
    min_data_latitude = data_latitude;
    // WINDOW MARGE
    margeX = margeX_init + viz_properties_float.get("node_diameter_max")/2.;
    margeY = margeY_init + viz_properties_float.get("node_diameter_max")/2.;
    // PARAMETERS OF THE STATIC AND DYNAMIC VISUALIZATION
    NODES_ARE_DRAWN = true;
    LINKS_ARE_DRAWN = true;
    LINK_WITH_ARROW = false;
    LEGEND_IS_DRAWN = false;
    BUFFER_LINKS_ARE_DRAWN = true;
    REFRESH_VIZ = true;
    DYNAMIC_VIZ = false;
    SAVE_DYNAMIC_VIZ = false;
    // FOCUS NODE
    focus_node = null;
    //
    loop();
  }
}

///////////////////////////////////////////////////////////////////////////////

void refresh_viz_properties() {
  // properties loading
  File file=new File(DATA_FOLDER_PATH + "/" + "viz_properties.json");
  boolean exists = file.exists();
  if (!exists) {
    println("JSONArray could not be parsed of file does not exist.");
    // TODO: add a message on the screen during data loading
  } else {
    data_properties = loadJSONArray(DATA_FOLDER_PATH + "/" + "viz_properties.json");
    for (int i = 0; i < data_properties.size(); i++) {
      JSONObject param = data_properties.getJSONObject(i);
      String id = param.getString("id");
      if (viz_properties_string.containsKey(id) == true) {
        String value = param.getString("value");
        viz_properties_string.put(id, value);
      } else if (viz_properties_integer.containsKey(id) == true) {
        int value = param.getInt("value");
        viz_properties_integer.put(id, value);
      } else if (viz_properties_float.containsKey(id) == true) {
        float value = param.getFloat("value");
        viz_properties_float.put(id, value);
      } else if (viz_properties_color.containsKey(id) == true) {
        String hex_value = "FF" + param.getString("value");
        color the_color_value = unhex(hex_value);
        viz_properties_color.put(id, the_color_value);
      } else if (viz_properties_stringArray.containsKey(id) == true) {
        JSONArray sub_param = param.getJSONArray("value");
        String[] values = sub_param.getStringArray();
        viz_properties_stringArray.put(id, values);
        //println(viz_properties_stringArray.get(id));
      } else if (viz_properties_HMcolor.containsKey(id) == true) {
        if (viz_properties_integer.get("index_column_type") > 3) {
          JSONArray sub_properties = param.getJSONArray("value");
          HashMap<String, Integer> values = new HashMap<String, Integer>();
          for (int j = 0; j < sub_properties.size(); j++) {
            JSONObject item = sub_properties.getJSONObject(j);
            String type = item.getString("type");
            String hex_value = "FF" + item.getString("col");
            color the_color_value = unhex(hex_value);
            values.put(type, the_color_value);
          }
          viz_properties_HMcolor.put(id, values);
        } else {
          String hex_value = "FF" + param.getString("value");
          color the_color_value = unhex(hex_value);
          HashMap<String, Integer> values = new HashMap<String, Integer>();
          values.put("NA", the_color_value);
          viz_properties_HMcolor.put(id, values);
        }
        //println(viz_properties_colorArray.get(id));
      } else {
        println("this key does not exist: ", id);
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////

void update_x_and_y_position() {
  if ( (mouseX >= exit_button_pos_X) && (mouseX <= exit_button_pos_X+exit_button_size) && (mouseY >= exit_button_pos_Y) && ( mouseY <= exit_button_pos_Y+exit_button_size) ) {
    mouseOverExitButton = true;
  } else {
    mouseOverExitButton = false;
  }
}

///////////////////////////////////////////////////////////////////////////////

boolean overRect(int x, int y, int width, int height) {
  if (mouseX >= x && mouseX <= x+width &&
    mouseY >= y && mouseY <= y+height) {
    return true;
  } else {
    return false;
  }
}

///////////////////////////////////////////////////////////////////////////////

void drawCurvedArrow(float x0, float y0, float x1, float y1) {
  PVector d = new PVector(x1 - x0, y1 - y0);
  d.normalize();
  float dist = dist(x0, y0, x1, y1);
  float angle = atan2(d.y, d.x);
  noFill();
  if (dist!=0) {
    strokeCap(ROUND);
    pushMatrix();
    translate(x0, y0);
    rotate(angle);
    bezier(0, 0, 0+1+(dist*0.25), 0-1-(dist*0.1),0+dist-1-(dist*0.25), 0-1-(dist*0.1), dist, 0);
    line(dist-5,0-8,dist,0);
    popMatrix();
  }
}

///////////////////////////////////////////////////////////////////////////////

void drawCurvedLine(float x0, float y0, float x1, float y1) {
  PVector d = new PVector(x1 - x0, y1 - y0);
  d.normalize();
  float dist = dist(x0, y0, x1, y1);
  float angle = atan2(d.y, d.x);
  noFill();
  if (dist!=0) {
    strokeCap(ROUND);
    pushMatrix();
    translate(x0, y0);
    rotate(angle);
    bezier(0, 0, 0+1+(dist*0.25), 0-1-(dist*0.1),0+dist-1-(dist*0.25), 0-1-(dist*0.1), dist, 0);
    popMatrix();
  }
}


//      ___           ___          _____
//     /  /\         /__/\        /  /::\
//    /  /:/_        \  \:\      /  /:/\:\
//   /  /:/ /\        \  \:\    /  /:/  \:\
//  /  /:/ /:/_   _____\__\:\  /__/:/ \__\:|
// /__/:/ /:/ /\ /__/::::::::\ \  \:\ /  /:/
// \  \:\/:/ /:/ \  \:\~~\~~\/  \  \:\  /:/
//  \  \::/ /:/   \  \:\  ~~~    \  \:\/:/
//   \  \:\/:/     \  \:\         \  \::/
//    \  \::/       \  \:\         \__\/
//     \__\/         \__\/
