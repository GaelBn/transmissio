// Transmissio - version 1.1

/**
 * Copyright Gael Beaunée (April 10th 2019)
 *
 * gael.beaunee@gmail.com
 *
 * This software is a computer program whose purpose is to visualize 
 * temporal graph data (network) combined with geographic information 
 * (node coordinates).
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 */


//     _____          ___                       ___     
//    /  /::\        /  /\          ___        /  /\    
//   /  /:/\:\      /  /::\        /  /\      /  /::\   
//  /  /:/  \:\    /  /:/\:\      /  /:/     /  /:/\:\  
// /__/:/ \__\:|  /  /:/~/::\    /  /:/     /  /:/~/::\ 
// \  \:\ /  /:/ /__/:/ /:/\:\  /  /::\    /__/:/ /:/\:\
//  \  \:\  /:/  \  \:\/:/__\/ /__/:/\:\   \  \:\/:/__\/
//   \  \:\/:/    \  \::/      \__\/  \:\   \  \::/     
//    \  \::/      \  \:\           \  \:\   \  \:\     
//     \__\/        \  \:\           \__\/    \  \:\    
//                   \__\/                     \__\/   
                   
void loadNodeIDs() {
  println("- Get node ID");
  // an alternative way to load the data, maybe less efficient than the next way
  //String[] lines = loadStrings(DATA_FOLDER_PATH + "/" + viz_properties_string.get("network_file_name") + ".csv");
  //println("there are " + lines.length + " lines");
  //for (int i = 0 ; i < lines.length; i++) {
  //  //println(lines[i]);
  //}
  //
  // create reader for the network data file
  READER_DATA_NETWORK = createReader(DATA_FOLDER_PATH + "/" + viz_properties_string.get("network_file_name") + ".csv");
  // skip the first line (the header line)
  try {
    THE_LINE_NET_DATA = READER_DATA_NETWORK.readLine();
  } catch (IOException e) {
    e.printStackTrace();
    THE_LINE_NET_DATA = null;
  }
  while (NODE_ID_LOADED == false) {
    try {
      THE_LINE_NET_DATA = READER_DATA_NETWORK.readLine();
    } catch (IOException e) {
      e.printStackTrace();
      THE_LINE_NET_DATA = null;
    }
    if (THE_LINE_NET_DATA == null) {
      NODE_ID_LOADED = true;
      println("reach End Of File! - " + number_of_lines + " lines");
    } else {
      number_of_lines += 1;
      String[] the_edge = split(THE_LINE_NET_DATA, ',');
      String source_id = the_edge[0];
      String destination_id = the_edge[1];
      String current_date_string = the_edge[2];
      // build the list of all node id
      list_id_of_all_nodes.add(source_id);
      list_id_of_all_nodes.add(destination_id);
      // find the first date in the data
      if (current_date_string.compareTo(FIRST_DATE_STR) < 0) { // current date (current_date_string) < FIRST_DATE_STR
        FIRST_DATE_STR = current_date_string;
      }
      index_node_id_loaded = list_id_of_all_nodes.size();
      // check date format
      if (viz_properties_string.get("temporal_scale").equals("timestep") == true) {
        int current_timestep = int(current_date_string);
        if (current_date_string.equals(str(current_timestep)) == true) {
          // They are equal, so this date is in timestep format
          continue;
        } else {
          // They are not equal, so this date is not in timestep format
          println("Not equal");
          date_pbm_timestep = true;
          break;
        } 
      } else {
        String[] m1 = match(current_date_string, "\\d{4}-\\d{2}-\\d{2}");
        if (m1 != null) {
          // date seems to be in date format
          String[] date_elements = splitTokens(current_date_string, "-");
          if((int(date_elements[1])>=1) & (int(date_elements[1])<=12) & (int(date_elements[2])>=1) & (int(date_elements[2])<=31)){
            continue; // date seems to be in date format and the month and the day appear to be in the right intervals
          } else {
            println("No match found");
            date_pbm_dateformat = true;
            break;
          }
        } else {
          println("No match found");
          date_pbm_dateformat = true;
          break;
        }
      }
    }
  }
}



// load boundaries geojson data

PShape buildChildShape(int i, int j) {
  // The PShape object
  // https://processing.org/reference/createShape_.html
  PShape sub_boundaries_shape;
  sub_boundaries_shape = createShape();
  sub_boundaries_shape.beginShape();
  //
  sub_boundaries_shape.fill(viz_properties_color.get("col_boundaries_shape_fill"));
  //
  if (viz_properties_float.get("boundaries_shape_strokeWeight") > 0.0) {
    sub_boundaries_shape.strokeWeight(viz_properties_float.get("boundaries_shape_strokeWeight"));
    sub_boundaries_shape.stroke(viz_properties_color.get("col_boundaries_shape_stroke"));
  } else {
    sub_boundaries_shape.noStroke();
  }
  //
  int k = 0;
  for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).size(); l++) {
    float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(0);
    float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(1);
    float proj_latitude = latitude;
    float proj_longitude = longitude;
    if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
      proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
      proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
    } else
    if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
      proj_latitude = latitude;
      proj_longitude = longitude;
    } else {
      println("Not a valid coordinates format!");
    }
    //
    float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
    float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);       
    //println("coord", posX, posY);
    sub_boundaries_shape.vertex(posX, posY);
  }
  if (boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).size() > 1) {
    for (k = 1; k < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).size(); k++) {
      sub_boundaries_shape.beginContour();
      for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).size(); l++) {
        float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(0);
        float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(k).getJSONArray(l).getDouble(1);
        float proj_latitude = latitude;
        float proj_longitude = longitude;
        if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
          proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
          proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
        } else
        if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
          proj_latitude = latitude;
          proj_longitude = longitude;
        } else {
          println("Not a valid coordinates format!");
        }
        //
        float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
        float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);       
        //println("coord", posX, posY);
        sub_boundaries_shape.vertex(posX, posY);
      }
      sub_boundaries_shape.endContour();
    }
  }
  //
  sub_boundaries_shape.endShape(CLOSE);
  //
  return sub_boundaries_shape;
}

PShape buildChildShape(int i) {
  // The PShape object
  // https://processing.org/reference/createShape_.html
  PShape sub_boundaries_shape;
  sub_boundaries_shape = createShape();
  sub_boundaries_shape.beginShape();
  //
  sub_boundaries_shape.fill(viz_properties_color.get("col_boundaries_shape_fill"));
  //
  if (viz_properties_float.get("boundaries_shape_strokeWeight") > 0.0) {
    sub_boundaries_shape.strokeWeight(viz_properties_float.get("boundaries_shape_strokeWeight"));
    sub_boundaries_shape.stroke(viz_properties_color.get("col_boundaries_shape_stroke"));
  } else {
    sub_boundaries_shape.noStroke();
  }
  //
  int j = 0;
  for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).size(); l++) {
    float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(0);
    float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(1);
    float proj_latitude = latitude;
    float proj_longitude = longitude;
    if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
      proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
      proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
    } else
    if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
      proj_latitude = latitude;
      proj_longitude = longitude;
    } else {
      println("Not a valid coordinates format!");
    }
    //
    float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
    float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);       
    //println("coord", posX, posY);
    sub_boundaries_shape.vertex(posX, posY);
  }
  if (boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").size() > 1) {
    for (j = 1; j < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").size(); j++) {
      sub_boundaries_shape.beginContour();
      for (int l = 0; l < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).size(); l++) {
        float longitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(0);
        float latitude = (float) boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").getJSONArray(j).getJSONArray(l).getDouble(1);
        float proj_latitude = latitude;
        float proj_longitude = longitude;
        if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
          proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
          proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
        } else
        if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
          proj_latitude = latitude;
          proj_longitude = longitude;
        } else {
          println("Not a valid coordinates format!");
        }
        //
        float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
        float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);       
        //println("coord", posX, posY);
        sub_boundaries_shape.vertex(posX, posY);
      }
      sub_boundaries_shape.endContour();
    }
  }
  //
  sub_boundaries_shape.endShape(CLOSE);
  //
  return sub_boundaries_shape;
}



// load boundaries geojson data

void loadBoundariesGeojsonData() {
  if (viz_properties_string.get("boundaries_geojson_file").equals("NA") == false) {
    String geojsonFilePath = DATA_FOLDER_PATH + "/" + viz_properties_string.get("boundaries_geojson_file");
    JSONObject json = loadJSONObject(geojsonFilePath);
    boundaries_geojson = json.getJSONArray("features");
    // https://processing.org/reference/createShape_.html
    // Create the shape group
    for (int i = 0 ; i < boundaries_geojson.size(); i++) {
      String type_of_polygon = boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getString("type");
      if (type_of_polygon.equals("MultiPolygon") == true) { // MultiPolygon
        for (int j = 0; j < boundaries_geojson.getJSONObject(i).getJSONObject("geometry").getJSONArray("coordinates").size(); j++) {
          // Build the PShape child object
          PShape chil_boundaries_shape = buildChildShape(i, j);
          // Add the "child" shapes to the parent group
          areaBoundariesShape.addChild(chil_boundaries_shape);
        }
      } else if (type_of_polygon.equals("Polygon") == true) { // Polygon
        // Build the PShape child object
        PShape chil_boundaries_shape = buildChildShape(i);
        // Add the "child" shapes to the parent group
        areaBoundariesShape.addChild(chil_boundaries_shape);
      }
    }
  }
} 



// compute variables that will be used to rescale node coordinates 

void rescaleNodesCoordinates() {
  println("- Rescale coordinates of the nodes");
  TableRow first_row = TABLE_DATA_COORD.getRow(0);
  max_data_longitude = first_row.getFloat("coord_longitude");
  min_data_longitude = first_row.getFloat("coord_longitude");
  max_data_latitude = first_row.getFloat("coord_latitude");
  min_data_latitude = first_row.getFloat("coord_latitude");
  for (String nodeID : list_id_of_all_nodes) {
    index_nodes_coordinates_rescaled++;
    // find min and max of longitude and latitude
    TableRow nodePos = TABLE_DATA_COORD.findRow(nodeID, "node_id");
    if (nodePos!=null) {
      float posX = nodePos.getFloat("coord_longitude");
      float posY = nodePos.getFloat("coord_latitude");
      max_data_longitude = max(max_data_longitude, posX);
      min_data_longitude = min(min_data_longitude, posX);
      max_data_latitude = max(max_data_latitude, posY);
      min_data_latitude = min(min_data_latitude, posY);
    }
  }
  //
  println("max_data_longitude: ", max_data_longitude);
  println("min_data_longitude: ", min_data_longitude);
  println("max_data_latitude: ", max_data_latitude);
  println("min_data_latitude: ", min_data_latitude);
  //
  mean_data_latitude = (min_data_latitude + max_data_latitude)/2.;
  mean_data_longitude = (min_data_longitude + max_data_longitude)/2.;
  //
  if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
    min_proj_latitude = ((min_data_latitude-mean_data_latitude)/360.)*TWO_PI;
    max_proj_latitude = ((max_data_latitude-mean_data_latitude)/360.)*TWO_PI;
    min_proj_longitude = ((min_data_longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
    max_proj_longitude = ((max_data_longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
  } else
  if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
    min_proj_latitude = min_data_latitude;
    max_proj_latitude = max_data_latitude;
    min_proj_longitude = min_data_longitude;
    max_proj_longitude = max_data_longitude;
  } else {
    println("Not a valid coordinates format!");
  }
  //
  float sketchWidth = width-(2*margeX);
  float sketchHeight = height-(2*margeY);
  //
  float visWidth = max_proj_longitude - min_proj_longitude;
  float visHeight = max_proj_latitude - min_proj_latitude;
  //
  if( (sketchWidth/sketchHeight) > (visWidth/visHeight) ){
    float newSketchWidth = (visWidth/visHeight)*sketchHeight;
    float diff = sketchWidth - newSketchWidth;
    margeX = margeX + (diff/2.);
  } else if( (sketchWidth/sketchHeight) < (visWidth/visHeight) ){
    float newSketchHeight = sketchWidth * (visHeight/visWidth);
    float diff = sketchHeight - newSketchHeight;
    margeY = margeY + (diff/2.);
  }
  //
  print("margeX: ");
  println(margeX);
  print("margeY: ");
  println(margeY);
}



// load network data

void loadNetworkData() {
  println("- Get the network data");
  // create reader for the network data file
  READER_DATA_NETWORK = createReader(DATA_FOLDER_PATH + "/" + viz_properties_string.get("network_file_name") + ".csv");
  // skip the first line (the header line)
  try {
    THE_LINE_NET_DATA = READER_DATA_NETWORK.readLine();
  } catch (IOException e) {
    e.printStackTrace();
    THE_LINE_NET_DATA = null;
  }
  boolean reachEndOfFile = false;
  boolean is_the_first_line = true;
  while (reachEndOfFile == false) {
    try {
      THE_LINE_NET_DATA = READER_DATA_NETWORK.readLine();
    } catch (IOException e) {
      e.printStackTrace();
      THE_LINE_NET_DATA = null;
    }
    if (THE_LINE_NET_DATA == null) {
      reachEndOfFile = true;
      println("Stop reading because of an error or file is empty! - " + number_of_lines + " mvts");
    } else {
      index_network_data_loaded++;
      if (index_network_data_loaded >= number_of_lines) {
        println("reach End Of File! - " + number_of_lines + " mvts");
        reachEndOfFile = true;
      }
      // split the informations
      String[] the_edge = split(THE_LINE_NET_DATA, ',');
      String source_id = the_edge[0];      
      String destination_id = the_edge[1];
      String current_date_string = the_edge[2];
      float theWeight = float(the_edge[3]);
      String theFlowType = "NA";
      if (viz_properties_integer.get("index_column_type") > 3) {
        theFlowType = the_edge[viz_properties_integer.get("index_column_type")];
      }
      // extract the date in date format
      Date current_date = new Date();
      if (viz_properties_string.get("temporal_scale").equals("timestep") == false) {
        try {
          current_date = new SimpleDateFormat("yyyy-MM-dd").parse(current_date_string);
        }
        catch (Exception e) {
        }
      }
      // create a calendar to store the date
      Calendar current_date_calendar = Calendar.getInstance();
      current_date_calendar.setTime(current_date);
      current_date_calendar.setMinimalDaysInFirstWeek(1);
      // calculate the corresponding time step based on the first date in the data
      int current_timestep = 0;
      // years
      if (viz_properties_string.get("temporal_scale").equals("timestep") == true) {
        current_timestep = int(current_date_string);
      } else
      if (viz_properties_string.get("temporal_scale").equals("year") == true) {
        int the_year = current_date_calendar.get(Calendar.YEAR);
        current_timestep = the_year - FIRST_DATE_YEAR + 1;
      } else
      if  (viz_properties_string.get("temporal_scale").equals("month") == true) {
        // months
        int the_year = current_date_calendar.get(Calendar.YEAR);
        int the_month = current_date_calendar.get(Calendar.MONTH)+1;
        current_timestep = the_month + (12 * (the_year - FIRST_DATE_YEAR)) + (FIRST_DATE_MONTH - 1);
      } else
      if  (viz_properties_string.get("temporal_scale").equals("week_num") == true) {
        // weeks (based on week number)
        int the_year = current_date_calendar.get(Calendar.YEAR);
        int the_week_of_the_year = current_date_calendar.get(Calendar.WEEK_OF_YEAR);
        // works because we set the minimal days in first week to 1 (setMinimalDaysInFirstWeek(1))
        current_timestep = the_week_of_the_year + (52 * (the_year - FIRST_DATE_YEAR)) + (FIRST_DATE_WEEK_OF_YEAR - 1);
      } else
      if  (viz_properties_string.get("temporal_scale").equals("seven_days") == true) {
        // weeks (based on period of seven days)
        current_timestep = ceil(int((current_date.getTime() - FIRST_DATE_SDF.getTime()) / (1000 * 60 * 60 * 24)) / 7.);
      } else
      if  (viz_properties_string.get("temporal_scale").equals("day") == true) {
        // days - diff between the to date in order to take into account leap year
        current_timestep = ceil(int((current_date.getTime() - FIRST_DATE_SDF.getTime()) / (1000 * 60 * 60 * 24)));
      } else {
        println("Not a valid temporal scale!");
      }
      // find the length of the period
      if (is_the_first_line) {
        min_timestep_viz = current_timestep;
        max_timestep_viz = current_timestep;
        is_the_first_line = false;
      }
      min_timestep_viz = min(min_timestep_viz, current_timestep);
      max_timestep_viz = max(max_timestep_viz, current_timestep);
      // check if node exist and create if needed - source
      if (hm_nodes.containsKey(source_id) == false & (list_id_of_node_without_coordinates.contains(source_id) == false)) {
        TableRow nodePos = TABLE_DATA_COORD.findRow(source_id, "node_id");
        if (nodePos!=null) {                  
          float latitude = nodePos.getFloat("coord_latitude");
          float longitude = nodePos.getFloat("coord_longitude");
          //
          float proj_latitude = latitude;
          float proj_longitude = longitude;
          if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
            proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
            proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
          } else
          if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
            proj_latitude = latitude;
            proj_longitude = longitude;
          } else {
            println("Not a valid coordinates format!");
          }
          //
          float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
          float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);
          // create new node with calculated coordinates (posX, posY)
          Node newNode = new Node(posX, posY, source_id);
          hm_nodes.put(source_id, newNode);
          list_id_of_node_with_coordinates.add(source_id);
        } else {
          //println("source_id without location: ", source_id);
          list_id_of_node_without_coordinates.add(source_id);
        }
      }
      // check if node exist and create if needed - destination
      if ((hm_nodes.containsKey(destination_id) == false) & (list_id_of_node_without_coordinates.contains(destination_id) == false)) {
        TableRow nodePos = TABLE_DATA_COORD.findRow(destination_id, "node_id");
        if (nodePos!=null) {          
          float latitude = nodePos.getFloat("coord_latitude");
          float longitude = nodePos.getFloat("coord_longitude");
          //
          float proj_latitude = latitude;
          float proj_longitude = longitude;
          if (viz_properties_string.get("coordinates_format").equals("gps") == true) {
            proj_latitude = ((latitude-mean_data_latitude)/360.)*TWO_PI;
            proj_longitude = ((longitude-mean_data_longitude)/360.)*TWO_PI*cos((mean_data_latitude/360.)*TWO_PI);
          } else
          if (viz_properties_string.get("coordinates_format").equals("projection") == true) {
            proj_latitude = latitude;
            proj_longitude = longitude;
          } else {
            println("Not a valid coordinates format!");
          }
          //
          float posX = map(proj_longitude, min_proj_longitude, max_proj_longitude, margeX, width-margeX);
          float posY = map(proj_latitude, min_proj_latitude, max_proj_latitude, height-margeY, margeY);
          // create new node with calculated coordinates (posX, posY)
          Node newNode = new Node(posX, posY, destination_id);
          hm_nodes.put(destination_id, newNode);
          list_id_of_node_with_coordinates.add(destination_id);
        } else {
          //println("destination_id without location: ", destination_id);
          list_id_of_node_without_coordinates.add(destination_id);
        }
      }
      // add edges
      if ((hm_nodes.containsKey(source_id) == true) & (source_id.equals("NA") == false)) {
        Node current_node = hm_nodes.get(source_id);
        if (destination_id.equals("NA") == false) {
          if (hm_nodes.containsKey(destination_id) == true) {
            if (current_node.hm_timestep_hm_destination_flow.containsKey(current_timestep) ) {
              if (current_node.hm_timestep_hm_destination_flow.get(current_timestep).containsKey(destination_id)) {
                current_node.hm_timestep_hm_destination_flow.get(current_timestep).put(destination_id, current_node.hm_timestep_hm_destination_flow.get(current_timestep).get(destination_id) + theWeight);
                //
                if (current_node.hm_timestep_hm_destination_hm_flowType_flow.get(current_timestep).get(destination_id).containsKey(theFlowType)) {
                  current_node.hm_timestep_hm_destination_hm_flowType_flow.get(current_timestep).get(destination_id).put(theFlowType, current_node.hm_timestep_hm_destination_hm_flowType_flow.get(current_timestep).get(destination_id).get(theFlowType) + theWeight);
                } else {
                  current_node.hm_timestep_hm_destination_hm_flowType_flow.get(current_timestep).get(destination_id).put(theFlowType, theWeight);
                }
              } else {
                current_node.hm_timestep_hm_destination_flow.get(current_timestep).put(destination_id, theWeight);
                //
                HashMap<String,Float> movs = new HashMap<String,Float>();
                movs.put(theFlowType, theWeight);
                current_node.hm_timestep_hm_destination_hm_flowType_flow.get(current_timestep).put(destination_id, movs);
              } 
            } else {
              HashMap<String,Float> movs = new HashMap<String,Float>();
              movs.put(destination_id, theWeight);
              current_node.hm_timestep_hm_destination_flow.put(current_timestep, movs);
              //
              HashMap<String,Float> type_movs = new HashMap<String,Float>();
              type_movs.put(theFlowType, theWeight);
              HashMap<String,HashMap<String,Float>> dest_type_movs = new HashMap<String,HashMap<String,Float>>();
              dest_type_movs.put(destination_id, type_movs);
              current_node.hm_timestep_hm_destination_hm_flowType_flow.put(current_timestep, dest_type_movs);
            }
          }
        }
        if (destination_id.equals("NA") == true) {
          // flow to na
          if (current_node.hm_timestep_flow_to_na.containsKey(current_timestep) ) {
            current_node.hm_timestep_flow_to_na.put(current_timestep, current_node.hm_timestep_flow_to_na.get(current_timestep) + theWeight);
          } else {
            current_node.hm_timestep_flow_to_na.put(current_timestep, theWeight);
          }
        }      
        hm_nodes.put(current_node.node_id, current_node);
      }
      if (source_id.equals("NA") == true) {
        // flow from na
        if (hm_nodes.containsKey(destination_id) == true) {
          Node current_node_destination = hm_nodes.get(destination_id);
          if (current_node_destination.hm_timestep_flow_from_na.containsKey(current_timestep) ) {
            current_node_destination.hm_timestep_flow_from_na.put(current_timestep, current_node_destination.hm_timestep_flow_from_na.get(current_timestep) + theWeight);
          } else {
            current_node_destination.hm_timestep_flow_from_na.put(current_timestep, theWeight);
          }
          hm_nodes.put(current_node_destination.node_id, current_node_destination);
        }
      }
    }
  }
  number_of_nodes = list_id_of_node_with_coordinates.size();
}
