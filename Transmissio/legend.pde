// Transmissio - version 1.1

/**
 * Copyright Gael Beaunée (April 10th 2019)
 *
 * gael.beaunee@gmail.com
 *
 * This software is a computer program whose purpose is to visualize 
 * temporal graph data (network) combined with geographic information 
 * (node coordinates).
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 */


void drawLegendGeneral() {
  
  float the_pos_x = margeX_init + 20;
  float the_pos_y = height - margeY_init - 30;
  
  float marge_x = 10;
  float marge_y = 10;
  
  float space_between_legend = 25; // 30
  
  float ellipse_inter_space = 35.; // 50.
  float link_inter_space = 50;
  float text_space = 6.; // 9.
  float text_height = 12; // 18
  
  float legend_width = 0;
  float legend_height = 0;
  
  float flow_legend_diam = computeNodeDiameter(max_of_in_and_out_flow, 0, 0, 0);
  float flow_type_legend_height = flow_legend_diam;
  float flow_type_legend_width = flow_legend_diam;
  textSize(text_height);
  flow_type_legend_width += max(textWidth("ingoing flow"), textWidth("outgoing flow"))*2;
  flow_type_legend_width += text_space*2;
  
  
  FloatList legend_item_size;
  legend_item_size = new FloatList();
  //legend_item_size.append(round(max_of_in_and_out_flow/32.));
  //legend_item_size.append(round(max_of_in_and_out_flow/8.));
  //legend_item_size.append(round(max_of_in_and_out_flow/4.));
  //legend_item_size.append(round(max_of_in_and_out_flow/2.));
  legend_item_size.append(round(max_of_in_and_out_flow/100.));
  legend_item_size.append(round(max_of_in_and_out_flow/25.));
  legend_item_size.append(round(max_of_in_and_out_flow/5.));
  legend_item_size.append(round(max_of_in_and_out_flow/2.5));
  legend_item_size.append(round(max_of_in_and_out_flow/(5./3.)));
  legend_item_size.append(round(max_of_in_and_out_flow));
  
  FloatList legend_link_size;
  legend_link_size = new FloatList();
  legend_link_size.append(1.0);
  legend_link_size.append(round(viz_properties_float.get("threshold_max_flow_size_strokeWeight")/10.));
  legend_link_size.append(round(viz_properties_float.get("threshold_max_flow_size_strokeWeight")/5.));
  legend_link_size.append(round(viz_properties_float.get("threshold_max_flow_size_strokeWeight")/2.));
  legend_link_size.append(viz_properties_float.get("threshold_max_flow_size_strokeWeight"));
  
  // get the height of the legend part concerning node size
  float the_max_diam = computeNodeDiameter(legend_item_size.max(), 0, 0, 0);
  float node_size_legend_height = the_max_diam + text_space + text_height;
  float link_size_legend_height = viz_properties_float.get("threshold_link_strokeWeight_max") + text_space + text_height;
  
  // get the width of the legend part concerning node size
  float node_size_legend_width = 0;
  // legend width based on the ellipses
  float node_size_legend_width_ellipses = 0;
  for (float the_size : legend_item_size) {
    float the_current_diam = computeNodeDiameter(the_size, 0, 0, 0);
    node_size_legend_width_ellipses += the_current_diam;
  }
  node_size_legend_width_ellipses += (legend_item_size.size()-1) * ellipse_inter_space;
  // legend width based on the text
  float node_size_legend_width_text = 0;
  textSize(text_height);
  node_size_legend_width_text = node_size_legend_width_ellipses;
  
  node_size_legend_width_text -= computeNodeDiameter(legend_item_size.get(0), 0, 0, 0)/2.;
  String first_item_text = nfc(legend_item_size.get(0), 0);
  node_size_legend_width_text += textWidth(first_item_text)/2.;
  
  node_size_legend_width_text -= computeNodeDiameter(legend_item_size.get(legend_item_size.size()-1), 0, 0, 0)/2.;
  String last_item_text = nfc(legend_item_size.get(legend_item_size.size()-1), 0);
  node_size_legend_width_text += textWidth(last_item_text)/2.;
  
  // find the max
  node_size_legend_width = max(node_size_legend_width_ellipses, node_size_legend_width_text);
  
  
  legend_width = max(node_size_legend_width, flow_type_legend_width);
  legend_height += flow_type_legend_height + node_size_legend_height + link_size_legend_height + space_between_legend*2;
  
  
  if (max_of_in_and_out_flow > 0.0) {
    // background of the legend
    fill(240, 150);
    noStroke();
    rect(the_pos_x-marge_x, the_pos_y-legend_height-marge_y, legend_width+2*marge_x, legend_height+2*marge_y);
    
    
    // draw flow type legend
    float flow_type_legend_x = the_pos_x + legend_width/2.;
    float flow_type_legend_y = the_pos_y - legend_height + flow_legend_diam/2.;
    drawTheNode(flow_type_legend_x, flow_type_legend_y, max_of_in_and_out_flow*(2./10.), max_of_in_and_out_flow*(3./10.), max_of_in_and_out_flow*(2./10.), max_of_in_and_out_flow*(3./10.));
    noStroke();
    textAlign(LEFT, CENTER);
    fill(viz_properties_color.get("col_node_outgoing_flow"));
    text("outgoing flow", flow_type_legend_x + (flow_legend_diam/2. + text_space), flow_type_legend_y);
    textAlign(RIGHT, CENTER);
    fill(viz_properties_color.get("col_node_ingoing_flow"));
    text("ingoing flow", flow_type_legend_x - (flow_legend_diam/2. + text_space), flow_type_legend_y);
    
    // draw node size legend
    float the_previous_diam = 0;
    float node_size_legend_x = the_pos_x + max(0, (node_size_legend_width_text-node_size_legend_width_ellipses)/2.);
    float node_size_legend_y = the_pos_y - link_size_legend_height - space_between_legend - node_size_legend_height + the_max_diam/2.;
    int legend_index = 0;
    for (float the_size : legend_item_size) {
      // ellipse
      float the_current_diam = computeNodeDiameter(the_size, 0, 0, 0);
      node_size_legend_x = node_size_legend_x + the_previous_diam/2. + the_current_diam/2.;
      drawTheNode(node_size_legend_x, node_size_legend_y, the_size, 0, 0, 0, color(30), color(30));
      // text
      textAlign(CENTER, TOP);
      fill(30);
      textSize(text_height);
      String fi = nfc(the_size, 0);
      text(fi, node_size_legend_x, node_size_legend_y + the_max_diam/2. + text_space);
      // update variable for next ellipse
      the_previous_diam = the_current_diam;
      node_size_legend_x += ellipse_inter_space;
    }
    
    // draw node size legend
    strokeCap(SQUARE);
    float link_size_legend_x = the_pos_x + (legend_width-link_inter_space*4)/2;
    float link_size_legend_y = the_pos_y - link_size_legend_height + viz_properties_float.get("threshold_link_strokeWeight_max")/2.;
    for (float the_size : legend_link_size) {
      // ellipse
      stroke(30);
      strokeWeight(map(min(the_size, viz_properties_float.get("threshold_max_flow_size_strokeWeight")),
              viz_properties_float.get("threshold_min_flow_size_strokeWeight"),
              viz_properties_float.get("threshold_max_flow_size_strokeWeight"),
              viz_properties_float.get("threshold_link_strokeWeight_min"),
              viz_properties_float.get("threshold_link_strokeWeight_max"))
              );
      line(link_size_legend_x-link_inter_space/4, link_size_legend_y, link_size_legend_x+link_inter_space/4, link_size_legend_y);
      // text
      textAlign(CENTER, TOP);
      fill(30);
      textSize(text_height);
      String fi = nfc(the_size, 0);
      text(fi, link_size_legend_x, link_size_legend_y + viz_properties_float.get("threshold_link_strokeWeight_max")/2. + text_space);
      //
      link_size_legend_x += link_inter_space;
    }
    strokeCap(ROUND);
  }
  
}


void drawLegendForFocusNode() {
  
  float the_pos_x = width - margeX_init - 10 - 75;
  float the_pos_y = margeY_init + 10;
  
  float text_height = 12;
  float space_between_item = 10;
  
  // background of the legend
  fill(240, 150);
  noStroke();
  rect(the_pos_x - 75, the_pos_y, 150, 150);
  
  fill(30);
  textSize(text_height);
  textAlign(CENTER, CENTER);
  
  the_pos_y += space_between_item/2.;
  
  the_pos_y += space_between_item + text_height/2.;
  text("ID: " + focus_node.getID(), the_pos_x, the_pos_y);
  
  float the_max_diam = computeNodeDiameter(max_of_in_and_out_flow, 0, 0, 0);
  the_pos_y += text_height/2 + space_between_item + the_max_diam/2.;
  drawTheNode(the_pos_x, the_pos_y, focus_node.ingoing_flow, focus_node.ingoing_flow_from_na, focus_node.outgoing_flow, focus_node.outgoing_flow_to_na);
  
  noStroke();
  
  the_pos_y += the_max_diam/2. + space_between_item + text_height/2.;
  float the_ratio = focus_node.get_outgoing_flow()/(focus_node.get_ingoing_flow() + focus_node.get_outgoing_flow());
  color col_inter = lerpColor(viz_properties_color.get("col_node_ingoing_flow"), viz_properties_color.get("col_node_outgoing_flow"), the_ratio);
  //fill(viz_properties_color.get("col_link_flow"));
  fill(col_inter);
  text("total flow: " + nfc(focus_node.get_ingoing_flow() + focus_node.get_outgoing_flow(), 0), the_pos_x, the_pos_y);
  
  the_pos_y += text_height/2. + space_between_item + text_height/2.;
  fill(viz_properties_color.get("col_node_ingoing_flow"));
  text("ingoing flow: " + nfc(focus_node.get_ingoing_flow(), 0), the_pos_x, the_pos_y);
  
  the_pos_y += text_height/2. + space_between_item + text_height/2.;
  fill(viz_properties_color.get("col_node_outgoing_flow"));
  text("outgoing flow: " + nfc(focus_node.get_outgoing_flow(), 0), the_pos_x, the_pos_y);
  
}
