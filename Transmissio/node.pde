// Transmissio - version 1.1

/**
 * Copyright Gael Beaunée (April 10th 2019)
 *
 * gael.beaunee@gmail.com
 *
 * This software is a computer program whose purpose is to visualize 
 * temporal graph data (network) combined with geographic information 
 * (node coordinates).
 *
 * This software is governed by the CeCILL-B license under French law and
 * abiding by the rules of distribution of free software.  You can  use, 
 * modify and/or redistribute the software under the terms of the CeCILL-B
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability. 
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and,  more generally, to use and operate it in the 
 * same conditions as regards security. 
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL-B license and that you accept its terms.
 */


//      ___           ___          _____          ___     
//     /__/\         /  /\        /  /::\        /  /\    
//     \  \:\       /  /::\      /  /:/\:\      /  /:/_   
//      \  \:\     /  /:/\:\    /  /:/  \:\    /  /:/ /\  
//  _____\__\:\   /  /:/  \:\  /__/:/ \__\:|  /  /:/ /:/_ 
// /__/::::::::\ /__/:/ \__\:\ \  \:\ /  /:/ /__/:/ /:/ /\
// \  \:\~~\~~\/ \  \:\ /  /:/  \  \:\  /:/  \  \:\/:/ /:/
//  \  \:\  ~~~   \  \:\  /:/    \  \:\/:/    \  \::/ /:/ 
//   \  \:\        \  \:\/:/      \  \::/      \  \:\/:/  
//    \  \:\        \  \::/        \__\/        \  \::/   
//     \__\/         \__\/                       \__\/    

class Node extends PVector {
  // ------ variables ------
  
  // id  
  String node_id = "";
  
  // hashmap to store the edges data
  HashMap<Integer,HashMap<String,Float>> hm_timestep_hm_destination_flow = new HashMap<Integer,HashMap<String,Float>>();
  HashMap<Integer,HashMap<String,HashMap<String,Float>>> hm_timestep_hm_destination_hm_flowType_flow = new HashMap<Integer,HashMap<String,HashMap<String,Float>>>();
  HashMap<Integer,Float> hm_timestep_flow_from_na = new HashMap<Integer,Float>();
  HashMap<Integer,Float> hm_timestep_flow_to_na = new HashMap<Integer,Float>();
  
  // in and out flow
  float ingoing_flow = 0;
  float outgoing_flow = 0;
  float ingoing_flow_from_na = 0;
  float outgoing_flow_to_na = 0;


  // ------ constructors ------
  
  public Node(float theX, float theY, String theID) {
    x = theX;
    y = theY;
    node_id = theID;
  }
  
  public Node(float theX, float theY) {
    x = theX;
    y = theY;
  }

  public Node(PVector theVector, String theID) {
    x = theVector.x;
    y = theVector.y;
    node_id = theID;
  }
  
  public Node(PVector theVector) {
    x = theVector.x;
    y = theVector.y;
  }
  
  
  // ------ methods ------
  
  void draw(){
    drawTheNode(x, y, ingoing_flow, ingoing_flow_from_na, outgoing_flow, outgoing_flow_to_na);
  }


  // ------ getters and setters ------
  
  public String getID() {
    return node_id;
  }

  public void setID(String theID) {
    this.node_id = theID;
  }
  
  public float get_ingoing_flow() {
    return ingoing_flow;
  }
  
  public float get_outgoing_flow() {
    return outgoing_flow;
  }
}


void drawTheNode(float pos_x, float pos_y, float the_ingoing_flow, float the_ingoing_flow_from_na, float the_outgoing_flow, float the_outgoing_flow_to_na){
  drawTheNode(pos_x, pos_y, the_ingoing_flow, the_ingoing_flow_from_na, the_outgoing_flow, the_outgoing_flow_to_na, viz_properties_color.get("col_node_ingoing_flow"), viz_properties_color.get("col_node_outgoing_flow"));
}


void drawTheNode(float pos_x, float pos_y, float the_ingoing_flow, float the_ingoing_flow_from_na, float the_outgoing_flow, float the_outgoing_flow_to_na, color ingoing_col, color outgoing_col){
  //
  float ingoing_flow_all = the_ingoing_flow + the_ingoing_flow_from_na;
  float outgoing_flow_all = the_outgoing_flow + the_outgoing_flow_to_na;
  float total_flow = ingoing_flow_all + outgoing_flow_all;
  //float diam = 2.*sqrt(((min(total_flow,max_of_in_and_out_flow)/max_of_in_and_out_flow) * (sq(viz_properties_float.get("node_diameter_max")/2.)*PI))/PI);
  float diam = 2.*sqrt((min(total_flow,max_of_in_and_out_flow)/max_of_in_and_out_flow) * sq(viz_properties_float.get("node_diameter_max")/2.));
  //diam = max(diam, 1.);
  float area = (diam/2.) * (diam/2.) * PI;
  //
  float the_alpha = viz_properties_float.get("node_transparency");
  // outgoing flow
  float prop_outgoing_flow_all = outgoing_flow_all / total_flow;
  float arc_prop_outgoing_flow_all = TWO_PI * prop_outgoing_flow_all;
  fill(outgoing_col, the_alpha);
  noStroke();
  arc(pos_x, pos_y, diam, diam, -arc_prop_outgoing_flow_all/2, arc_prop_outgoing_flow_all/2);
  // if flow to na
  if (the_outgoing_flow < outgoing_flow_all) {
    float flow_to_not_na = the_outgoing_flow / outgoing_flow_all;
    float diam_to_not_na = 2. * sqrt((area * flow_to_not_na) / PI);
    //stroke(0,200);
    //strokeWeight(0.5);
    noStroke();
    fill(255,75);
    //fill(outgoing_col, the_alpha);
    arc(pos_x, pos_y, diam_to_not_na, diam_to_not_na, -arc_prop_outgoing_flow_all/2, arc_prop_outgoing_flow_all/2);
  }
  // ingoing flow
  float prop_ingoing_flow_all = ingoing_flow_all / total_flow;
  fill(ingoing_col, the_alpha);
  noStroke();
  arc(pos_x, pos_y, diam, diam, arc_prop_outgoing_flow_all/2, (arc_prop_outgoing_flow_all/2)+(TWO_PI*prop_ingoing_flow_all));
  // if flow from na
  if (the_ingoing_flow < ingoing_flow_all) {
    float flow_from_not_na = the_ingoing_flow / ingoing_flow_all;
    float diam_from_not_na = 2. * sqrt((area * (flow_from_not_na)) / PI);
    //stroke(0,200);
    //strokeWeight(0.5);
    noStroke();
    fill(255,75);
    //fill(ingoing_col, the_alpha);
    arc(pos_x, pos_y, diam_from_not_na, diam_from_not_na, arc_prop_outgoing_flow_all/2, (arc_prop_outgoing_flow_all/2)+(TWO_PI*prop_ingoing_flow_all));
  }
}


float computeNodeDiameter(float the_ingoing_flow, float the_ingoing_flow_from_na, float the_outgoing_flow, float the_outgoing_flow_to_na){
  float ingoing_flow_all = the_ingoing_flow + the_ingoing_flow_from_na;
  float outgoing_flow_all = the_outgoing_flow + the_outgoing_flow_to_na;
  float total_flow = ingoing_flow_all + outgoing_flow_all;
  //float diam = 2.*sqrt(((min(total_flow,max_of_in_and_out_flow)/max_of_in_and_out_flow) * (sq(viz_properties_float.get("node_diameter_max")/2.)*PI))/PI);
  float diam = 2.*sqrt((min(total_flow,max_of_in_and_out_flow)/max_of_in_and_out_flow) * sq(viz_properties_float.get("node_diameter_max")/2.));
  //diam = max(diam, 1.);
  //
  if (Float.isNaN(diam)) {
    diam = 0.0;
  }
  //
  return diam;
}
